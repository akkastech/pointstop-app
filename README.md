#POINTSTOP

## How to run with CLI

Make sure Node.js and Nativescript is installed in system.

```bash
$ npm install
```
To run on Android device *(Andorid Device or emulator must be running)*
```bash
$ tns platform add android
$ tns run andoird
```
To run on iOS emulator
```bash
$ tns platform add ios
$ tns run ios
```
