"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var phone_component_1 = require("./pages/phone/phone.component");
var walk_component_1 = require("./pages/walk/walk.component");
var agree_component_1 = require("./pages/agree/agree.component");
var home_component_1 = require("./pages/home/home.component");
var campaign_component_1 = require("./pages/campaign/campaign.component");
var campaigndetails_component_1 = require("./pages/campaigndetails/campaigndetails.component");
var routes = [
    { path: "", redirectTo: "/phone", pathMatch: "full" },
    { path: "phone", component: phone_component_1.PhoneComponent },
    { path: "walk", component: walk_component_1.WalkComponent },
    { path: "agree", component: agree_component_1.AgreeComponent },
    { path: "home", component: home_component_1.HomeComponent },
    { path: "campaign", component: campaign_component_1.CampaignComponent },
    { path: "campaigndetails", component: campaigndetails_component_1.CampaigndetailsComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhcHAucm91dGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5QztBQUN6QyxzREFBdUU7QUFHdkUsaUVBQStEO0FBQy9ELDhEQUE0RDtBQUM1RCxpRUFBK0Q7QUFDL0QsOERBQTREO0FBQzVELDBFQUF3RTtBQUN4RSwrRkFBNkY7QUFFN0YsSUFBTSxNQUFNLEdBQVc7SUFDckIsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRTtJQUNyRCxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGdDQUFjLEVBQUU7SUFDNUMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSw4QkFBYSxFQUFFO0lBQzFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsZ0NBQWMsRUFBRTtJQUM1QyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLDhCQUFhLEVBQUU7SUFDMUMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxzQ0FBaUIsRUFBRTtJQUNsRCxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsb0RBQXdCLEVBQUU7Q0FDakUsQ0FBQztBQU1GO0lBQUE7SUFBK0IsQ0FBQztJQUFuQixnQkFBZ0I7UUFKNUIsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFLENBQUMsaUNBQXdCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25ELE9BQU8sRUFBRSxDQUFDLGlDQUF3QixDQUFDO1NBQ3BDLENBQUM7T0FDVyxnQkFBZ0IsQ0FBRztJQUFELHVCQUFDO0NBQUEsQUFBaEMsSUFBZ0M7QUFBbkIsNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IFBob25lQ29tcG9uZW50IH0gZnJvbSBcIi4vcGFnZXMvcGhvbmUvcGhvbmUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IFdhbGtDb21wb25lbnQgfSBmcm9tIFwiLi9wYWdlcy93YWxrL3dhbGsuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IEFncmVlQ29tcG9uZW50IH0gZnJvbSBcIi4vcGFnZXMvYWdyZWUvYWdyZWUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tIFwiLi9wYWdlcy9ob21lL2hvbWUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IENhbXBhaWduQ29tcG9uZW50IH0gZnJvbSBcIi4vcGFnZXMvY2FtcGFpZ24vY2FtcGFpZ24uY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IENhbXBhaWduZGV0YWlsc0NvbXBvbmVudCB9IGZyb20gXCIuL3BhZ2VzL2NhbXBhaWduZGV0YWlscy9jYW1wYWlnbmRldGFpbHMuY29tcG9uZW50XCI7XHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICB7IHBhdGg6IFwiXCIsIHJlZGlyZWN0VG86IFwiL3Bob25lXCIsIHBhdGhNYXRjaDogXCJmdWxsXCIgfSxcclxuICB7IHBhdGg6IFwicGhvbmVcIiwgY29tcG9uZW50OiBQaG9uZUNvbXBvbmVudCB9LFxyXG4gIHsgcGF0aDogXCJ3YWxrXCIsIGNvbXBvbmVudDogV2Fsa0NvbXBvbmVudCB9LFxyXG4gIHsgcGF0aDogXCJhZ3JlZVwiLCBjb21wb25lbnQ6IEFncmVlQ29tcG9uZW50IH0sXHJcbiAgeyBwYXRoOiBcImhvbWVcIiwgY29tcG9uZW50OiBIb21lQ29tcG9uZW50IH0sXHJcbiAgeyBwYXRoOiBcImNhbXBhaWduXCIsIGNvbXBvbmVudDogQ2FtcGFpZ25Db21wb25lbnQgfSxcclxuICB7IHBhdGg6IFwiY2FtcGFpZ25kZXRhaWxzXCIsIGNvbXBvbmVudDogQ2FtcGFpZ25kZXRhaWxzQ29tcG9uZW50IH1cclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZS5mb3JSb290KHJvdXRlcyldLFxyXG4gIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBSb3V0aW5nTW9kdWxlIHt9XHJcbiJdfQ==