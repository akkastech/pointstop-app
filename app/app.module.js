"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var forms_1 = require("nativescript-angular/forms");
var http_1 = require("nativescript-angular/http");
var common_1 = require("nativescript-angular/common");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var angular_1 = require("nativescript-checkbox/angular");
var angular_2 = require("nativescript-telerik-ui/listview/angular");
var item_service_1 = require("./item/item.service");
var phone_component_1 = require("./pages/phone/phone.component");
var walk_component_1 = require("./pages/walk/walk.component");
var agree_component_1 = require("./pages/agree/agree.component");
var home_component_1 = require("./pages/home/home.component");
var campaign_component_1 = require("./pages/campaign/campaign.component");
var campaigndetails_component_1 = require("./pages/campaigndetails/campaigndetails.component");
var platform = require("platform");
var frame_1 = require("ui/frame");
var platform_1 = require("platform");
if (platform.isIOS) {
    GMSServices.provideAPIKey("AIzaSyA5tgfNMBaVqVkMR3eADdE_FcbYsCfcX_U");
}
function loaded(args) {
    if (platform_1.isIOS) {
        var navigationBar = frame_1.topmost().ios.controller.navigationBar;
        navigationBar.barStyle = 1;
    }
}
exports.loaded = loaded;
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";
// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [app_component_1.AppComponent],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                angular_1.TNSCheckBoxModule,
                angular_2.NativeScriptUIListViewModule,
                http_1.NativeScriptHttpModule,
                common_1.NativeScriptCommonModule
            ],
            declarations: [
                app_component_1.AppComponent,
                walk_component_1.WalkComponent,
                phone_component_1.PhoneComponent,
                agree_component_1.AgreeComponent,
                home_component_1.HomeComponent,
                campaign_component_1.CampaignComponent,
                campaigndetails_component_1.CampaigndetailsComponent
            ],
            providers: [item_service_1.ItemService],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBQzlFLG9EQUFxRTtBQUNyRSxrREFBbUU7QUFDbkUsc0RBQXVFO0FBQ3ZFLDZDQUFpRDtBQUNqRCxpREFBK0M7QUFDL0MseURBQWtFO0FBQ2xFLG9FQUF3RjtBQUV4RixvREFBa0Q7QUFDbEQsaUVBQStEO0FBQy9ELDhEQUE0RDtBQUM1RCxpRUFBK0Q7QUFDL0QsOERBQTREO0FBQzVELDBFQUF3RTtBQUN4RSwrRkFBNkY7QUFFN0YsbUNBQXFDO0FBSXJDLGtDQUFtQztBQUNuQyxxQ0FBaUM7QUFFakMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbkIsV0FBVyxDQUFDLGFBQWEsQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO0FBQ3ZFLENBQUM7QUFFRCxnQkFBdUIsSUFBZTtJQUNwQyxFQUFFLENBQUMsQ0FBQyxnQkFBSyxDQUFDLENBQUMsQ0FBQztRQUNWLElBQUksYUFBYSxHQUFHLGVBQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBQzNELGFBQWEsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO0lBQzdCLENBQUM7QUFDSCxDQUFDO0FBTEQsd0JBS0M7QUFFRCwyRUFBMkU7QUFDM0Usd0VBQXdFO0FBRXhFLDZFQUE2RTtBQUM3RSxzRUFBc0U7QUF5QnRFO0lBQUE7SUFHaUIsQ0FBQztJQUFaLFNBQVM7UUExQmQsZUFBUSxDQUFDO1lBQ1IsU0FBUyxFQUFFLENBQUMsNEJBQVksQ0FBQztZQUN6QixPQUFPLEVBQUU7Z0JBQ1Asd0NBQWtCO2dCQUNsQiw4QkFBZ0I7Z0JBQ2hCLCtCQUF1QjtnQkFDdkIsMkJBQWlCO2dCQUNqQixzQ0FBNEI7Z0JBQzVCLDZCQUFzQjtnQkFDdEIsaUNBQXdCO2FBQ3pCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLDRCQUFZO2dCQUNaLDhCQUFhO2dCQUNiLGdDQUFjO2dCQUNkLGdDQUFjO2dCQUNkLDhCQUFhO2dCQUNiLHNDQUFpQjtnQkFDakIsb0RBQXdCO2FBQ3pCO1lBQ0QsU0FBUyxFQUFFLENBQUMsMEJBQVcsQ0FBQztZQUN4QixPQUFPLEVBQUUsQ0FBQyx1QkFBZ0IsQ0FBQztTQUM1QixDQUFDO09BSUksU0FBUyxDQUFHO0lBQUQsZ0JBQUM7Q0FBQSxBQUhsQixJQUdrQjtBQUFaLDhCQUFTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2h0dHBcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBBcHBSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vYXBwLnJvdXRpbmdcIjtcclxuaW1wb3J0IHsgQXBwQ29tcG9uZW50IH0gZnJvbSBcIi4vYXBwLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBUTlNDaGVja0JveE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtY2hlY2tib3gvYW5ndWxhclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSUxpc3RWaWV3TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC10ZWxlcmlrLXVpL2xpc3R2aWV3L2FuZ3VsYXJcIjtcclxuXHJcbmltcG9ydCB7IEl0ZW1TZXJ2aWNlIH0gZnJvbSBcIi4vaXRlbS9pdGVtLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUGhvbmVDb21wb25lbnQgfSBmcm9tIFwiLi9wYWdlcy9waG9uZS9waG9uZS5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgV2Fsa0NvbXBvbmVudCB9IGZyb20gXCIuL3BhZ2VzL3dhbGsvd2Fsay5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgQWdyZWVDb21wb25lbnQgfSBmcm9tIFwiLi9wYWdlcy9hZ3JlZS9hZ3JlZS5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gXCIuL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgQ2FtcGFpZ25Db21wb25lbnQgfSBmcm9tIFwiLi9wYWdlcy9jYW1wYWlnbi9jYW1wYWlnbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgQ2FtcGFpZ25kZXRhaWxzQ29tcG9uZW50IH0gZnJvbSBcIi4vcGFnZXMvY2FtcGFpZ25kZXRhaWxzL2NhbXBhaWduZGV0YWlscy5jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCAqIGFzIHBsYXRmb3JtIGZyb20gXCJwbGF0Zm9ybVwiO1xyXG5kZWNsYXJlIHZhciBHTVNTZXJ2aWNlczogYW55O1xyXG5cclxuaW1wb3J0IHsgRXZlbnREYXRhIH0gZnJvbSBcImRhdGEvb2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQgeyB0b3Btb3N0IH0gZnJvbSBcInVpL2ZyYW1lXCI7XHJcbmltcG9ydCB7IGlzSU9TIH0gZnJvbSBcInBsYXRmb3JtXCI7XHJcblxyXG5pZiAocGxhdGZvcm0uaXNJT1MpIHtcclxuICBHTVNTZXJ2aWNlcy5wcm92aWRlQVBJS2V5KFwiQUl6YVN5QTV0Z2ZOTUJhVnFWa01SM2VBRGRFX0ZjYllzQ2ZjWF9VXCIpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbG9hZGVkKGFyZ3M6IEV2ZW50RGF0YSkge1xyXG4gIGlmIChpc0lPUykge1xyXG4gICAgbGV0IG5hdmlnYXRpb25CYXIgPSB0b3Btb3N0KCkuaW9zLmNvbnRyb2xsZXIubmF2aWdhdGlvbkJhcjtcclxuICAgIG5hdmlnYXRpb25CYXIuYmFyU3R5bGUgPSAxO1xyXG4gIH1cclxufVxyXG5cclxuLy8gVW5jb21tZW50IGFuZCBhZGQgdG8gTmdNb2R1bGUgaW1wb3J0cyBpZiB5b3UgbmVlZCB0byB1c2UgdHdvLXdheSBiaW5kaW5nXHJcbi8vIGltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2Zvcm1zXCI7XHJcblxyXG4vLyBVbmNvbW1lbnQgYW5kIGFkZCB0byBOZ01vZHVsZSBpbXBvcnRzICBpZiB5b3UgbmVlZCB0byB1c2UgdGhlIEhUVFAgd3JhcHBlclxyXG4vLyBpbXBvcnQgeyBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2h0dHBcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgYm9vdHN0cmFwOiBbQXBwQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXHJcbiAgICBBcHBSb3V0aW5nTW9kdWxlLFxyXG4gICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXHJcbiAgICBUTlNDaGVja0JveE1vZHVsZSxcclxuICAgIE5hdGl2ZVNjcmlwdFVJTGlzdFZpZXdNb2R1bGUsXHJcbiAgICBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlLFxyXG4gICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEFwcENvbXBvbmVudCxcclxuICAgIFdhbGtDb21wb25lbnQsXHJcbiAgICBQaG9uZUNvbXBvbmVudCxcclxuICAgIEFncmVlQ29tcG9uZW50LFxyXG4gICAgSG9tZUNvbXBvbmVudCxcclxuICAgIENhbXBhaWduQ29tcG9uZW50LFxyXG4gICAgQ2FtcGFpZ25kZXRhaWxzQ29tcG9uZW50XHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtJdGVtU2VydmljZV0sXHJcbiAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdXHJcbn0pXHJcbmV4cG9ydCAvKlxyXG5QYXNzIHlvdXIgYXBwbGljYXRpb24gbW9kdWxlIHRvIHRoZSBib290c3RyYXBNb2R1bGUgZnVuY3Rpb24gbG9jYXRlZCBpbiBtYWluLnRzIHRvIHN0YXJ0IHlvdXIgYXBwXHJcbiovXHJcbmNsYXNzIEFwcE1vZHVsZSB7fVxyXG4iXX0=