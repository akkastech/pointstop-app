import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { PhoneComponent } from "./pages/phone/phone.component";
import { WalkComponent } from "./pages/walk/walk.component";
import { AgreeComponent } from "./pages/agree/agree.component";
import { HomeComponent } from "./pages/home/home.component";
import { CampaignComponent } from "./pages/campaign/campaign.component";
import { CampaigndetailsComponent } from "./pages/campaigndetails/campaigndetails.component";

const routes: Routes = [
  { path: "", redirectTo: "/phone", pathMatch: "full" },
  { path: "phone", component: PhoneComponent },
  { path: "walk", component: WalkComponent },
  { path: "agree", component: AgreeComponent },
  { path: "home", component: HomeComponent },
  { path: "campaign", component: CampaignComponent },
  { path: "campaigndetails", component: CampaigndetailsComponent }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
