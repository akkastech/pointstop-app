import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { TNSCheckBoxModule } from "nativescript-checkbox/angular";
import { NativeScriptUIListViewModule } from "nativescript-telerik-ui/listview/angular";

import { ItemService } from "./item/item.service";
import { PhoneComponent } from "./pages/phone/phone.component";
import { WalkComponent } from "./pages/walk/walk.component";
import { AgreeComponent } from "./pages/agree/agree.component";
import { HomeComponent } from "./pages/home/home.component";
import { CampaignComponent } from "./pages/campaign/campaign.component";
import { CampaigndetailsComponent } from "./pages/campaigndetails/campaigndetails.component";

import * as platform from "platform";
declare var GMSServices: any;

import { EventData } from "data/observable";
import { topmost } from "ui/frame";
import { isIOS } from "platform";

if (platform.isIOS) {
  GMSServices.provideAPIKey("AIzaSyA5tgfNMBaVqVkMR3eADdE_FcbYsCfcX_U");
}

export function loaded(args: EventData) {
  if (isIOS) {
    let navigationBar = topmost().ios.controller.navigationBar;
    navigationBar.barStyle = 1;
  }
}

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    NativeScriptFormsModule,
    TNSCheckBoxModule,
    NativeScriptUIListViewModule,
    NativeScriptHttpModule,
    NativeScriptCommonModule
  ],
  declarations: [
    AppComponent,
    WalkComponent,
    PhoneComponent,
    AgreeComponent,
    HomeComponent,
    CampaignComponent,
    CampaigndetailsComponent
  ],
  providers: [ItemService],
  schemas: [NO_ERRORS_SCHEMA]
})
export /*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
class AppModule {}
