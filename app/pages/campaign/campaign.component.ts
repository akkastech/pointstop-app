import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { registerElement } from "nativescript-angular";
import { Page } from "ui/page";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { MapView, Marker, Position, Style } from "nativescript-google-maps-sdk";
import { Http } from "@angular/http";
import "rxjs/Rx";
import { Slider } from "ui/slider";

registerElement("MapView2", () => MapView);

@Component({
  selector: "my-app",
  templateUrl: "pages/campaign/campaign.html",
  styleUrls: ["pages/campaign/campaign-common.css"]
})
export class CampaignComponent implements OnInit {
  @ViewChild("container") container: ElementRef;
  @ViewChild("CB1") FirstCheckBox: ElementRef;

  lastCamera: String;

  marker = new Marker();

  public latitude = -33.86;
  public longitude = 151.2;
  zoom = 14;
  bearing = 0;
  tilt = 0;
  padding = [40, 40, 40, 40];
  mapView: MapView;

  public searchResults = [];
  public startLocationAddress;
  public place = "";

  styles = `[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 33
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f7f7f7"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#deecdb"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": "25"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "25"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-90"
            },
            {
                "lightness": "25"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e0f1f9"
            }
        ]
    }
]`;

  constructor(
    private router: Router,
    private page: Page,
    private routerExtensions: RouterExtensions,
    private http: Http
  ) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  nextPage() {
    this.router.navigate(["/campaigndetails"]);
  }

  public goBack() {
    this.routerExtensions.back();
  }

  //Map events
  onMapReady(event) {
    console.log("Map Ready");

    this.mapView = event.object;
    this.mapView.setStyle(<Style>JSON.parse(this.styles));
    console.log("Setting a marker...");

    //var marker = new Marker();
    this.marker.position = Position.positionFromLatLng(-33.86, 151.2);
    this.marker.title = "Sydney";
    this.marker.draggable = true;
    this.marker.snippet = "Australia";
    this.marker.userData = { index: 1 };
    this.marker.icon = "x102icon";

    this.mapView.addMarker(this.marker);
  }

  onCoordinateTapped(args) {
    this.marker.position = args.position;
    this.longitude = args.position.longitude;
    this.latitude = args.position.latitude;
    this.mapView.addMarker(this.marker);
    console.log(
      "Coordinate Tapped, Lat: " +
        args.position.latitude +
        ", Lon: " +
        args.position.longitude,
      args
    );
  }

  onMarkerEvent(args) {
    console.log(
      "Marker Event: '" +
        args.eventName +
        "' triggered on: " +
        args.marker.title +
        ", Lat: " +
        args.marker.position.latitude +
        ", Lon: " +
        args.marker.position.longitude,
      args
    );
  }

  onCameraChanged(args) {
    console.log(
      "Camera changed: " + JSON.stringify(args.camera),
      JSON.stringify(args.camera) === this.lastCamera
    );
    this.lastCamera = JSON.stringify(args.camera);
  }
  locationSearchFromInput(searchWord) {
    console.dir(searchWord.value);
    const placesUrl =
      "https://maps.googleapis.com/maps/api/place/autocomplete/json?";

    return new Promise((resolve, reject) => {
      this.http
        .get(
          `${placesUrl}input=${encodeURI(
            searchWord.value
          )}&types=geocode&key=AIzaSyAGAFTiovi1XGloeeMUwwTO9vPzgUVi3VE`
        )
        .subscribe(res => {
          //console.dir(res.json());
          resolve(
            res.json().predictions.map(place => {
              place.description;
              console.dir(place);
            })
          );
        });
    });
  }

  selectPlaceFromAutocompleteList(placeIndex) {
    this.startLocationAddress = this.searchResults[placeIndex];
    this.searchResults = [];
  }

  public onSecondSliderChange(args) {
    let slider = <Slider>args.object;

    //this.fontSize = slider.value;
  }
}
