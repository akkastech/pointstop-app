"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var page_1 = require("ui/page");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var nativescript_google_maps_sdk_1 = require("nativescript-google-maps-sdk");
var http_1 = require("@angular/http");
require("rxjs/Rx");
nativescript_angular_1.registerElement("MapView2", function () { return nativescript_google_maps_sdk_1.MapView; });
var CampaignComponent = (function () {
    function CampaignComponent(router, page, routerExtensions, http) {
        this.router = router;
        this.page = page;
        this.routerExtensions = routerExtensions;
        this.http = http;
        this.marker = new nativescript_google_maps_sdk_1.Marker();
        this.latitude = -33.86;
        this.longitude = 151.2;
        this.zoom = 14;
        this.bearing = 0;
        this.tilt = 0;
        this.padding = [40, 40, 40, 40];
        this.searchResults = [];
        this.place = "";
        this.styles = "[\n    {\n        \"featureType\": \"administrative\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            },\n            {\n                \"lightness\": 33\n            }\n        ]\n    },\n    {\n        \"featureType\": \"landscape\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"color\": \"#f7f7f7\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"poi.business\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"poi.park\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#deecdb\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"poi.park\",\n        \"elementType\": \"labels\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            },\n            {\n                \"lightness\": \"25\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"lightness\": \"25\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road\",\n        \"elementType\": \"labels.icon\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.highway\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#ffffff\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.highway\",\n        \"elementType\": \"labels\",\n        \"stylers\": [\n            {\n                \"saturation\": \"-90\"\n            },\n            {\n                \"lightness\": \"25\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.arterial\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.arterial\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#ffffff\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.local\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#ffffff\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"transit.line\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"transit.station\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"water\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            },\n            {\n                \"color\": \"#e0f1f9\"\n            }\n        ]\n    }\n]";
    }
    CampaignComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    CampaignComponent.prototype.nextPage = function () {
        this.router.navigate(["/campaigndetails"]);
    };
    CampaignComponent.prototype.goBack = function () {
        this.routerExtensions.back();
    };
    //Map events
    CampaignComponent.prototype.onMapReady = function (event) {
        console.log("Map Ready");
        this.mapView = event.object;
        this.mapView.setStyle(JSON.parse(this.styles));
        console.log("Setting a marker...");
        //var marker = new Marker();
        this.marker.position = nativescript_google_maps_sdk_1.Position.positionFromLatLng(-33.86, 151.2);
        this.marker.title = "Sydney";
        this.marker.draggable = true;
        this.marker.snippet = "Australia";
        this.marker.userData = { index: 1 };
        this.marker.icon = "x102icon";
        this.mapView.addMarker(this.marker);
    };
    CampaignComponent.prototype.onCoordinateTapped = function (args) {
        this.marker.position = args.position;
        this.longitude = args.position.longitude;
        this.latitude = args.position.latitude;
        this.mapView.addMarker(this.marker);
        console.log("Coordinate Tapped, Lat: " +
            args.position.latitude +
            ", Lon: " +
            args.position.longitude, args);
    };
    CampaignComponent.prototype.onMarkerEvent = function (args) {
        console.log("Marker Event: '" +
            args.eventName +
            "' triggered on: " +
            args.marker.title +
            ", Lat: " +
            args.marker.position.latitude +
            ", Lon: " +
            args.marker.position.longitude, args);
    };
    CampaignComponent.prototype.onCameraChanged = function (args) {
        console.log("Camera changed: " + JSON.stringify(args.camera), JSON.stringify(args.camera) === this.lastCamera);
        this.lastCamera = JSON.stringify(args.camera);
    };
    CampaignComponent.prototype.locationSearchFromInput = function (searchWord) {
        var _this = this;
        console.dir(searchWord.value);
        var placesUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
        return new Promise(function (resolve, reject) {
            _this.http
                .get(placesUrl + "input=" + encodeURI(searchWord.value) + "&types=geocode&key=AIzaSyAGAFTiovi1XGloeeMUwwTO9vPzgUVi3VE")
                .subscribe(function (res) {
                //console.dir(res.json());
                resolve(res.json().predictions.map(function (place) {
                    place.description;
                    console.dir(place);
                }));
            });
        });
    };
    CampaignComponent.prototype.selectPlaceFromAutocompleteList = function (placeIndex) {
        this.startLocationAddress = this.searchResults[placeIndex];
        this.searchResults = [];
    };
    CampaignComponent.prototype.onSecondSliderChange = function (args) {
        var slider = args.object;
        //this.fontSize = slider.value;
    };
    __decorate([
        core_1.ViewChild("container"),
        __metadata("design:type", core_1.ElementRef)
    ], CampaignComponent.prototype, "container", void 0);
    __decorate([
        core_1.ViewChild("CB1"),
        __metadata("design:type", core_1.ElementRef)
    ], CampaignComponent.prototype, "FirstCheckBox", void 0);
    CampaignComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "pages/campaign/campaign.html",
            styleUrls: ["pages/campaign/campaign-common.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router,
            page_1.Page,
            router_2.RouterExtensions,
            http_1.Http])
    ], CampaignComponent);
    return CampaignComponent;
}());
exports.CampaignComponent = CampaignComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FtcGFpZ24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2FtcGFpZ24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLDZEQUF1RDtBQUN2RCxnQ0FBK0I7QUFDL0IsMENBQXlDO0FBQ3pDLHNEQUErRDtBQUMvRCw2RUFBZ0Y7QUFDaEYsc0NBQXFDO0FBQ3JDLG1CQUFpQjtBQUdqQixzQ0FBZSxDQUFDLFVBQVUsRUFBRSxjQUFNLE9BQUEsc0NBQU8sRUFBUCxDQUFPLENBQUMsQ0FBQztBQU8zQztJQTBLRSwyQkFDVSxNQUFjLEVBQ2QsSUFBVSxFQUNWLGdCQUFrQyxFQUNsQyxJQUFVO1FBSFYsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFNBQUksR0FBSixJQUFJLENBQU07UUF4S3BCLFdBQU0sR0FBRyxJQUFJLHFDQUFNLEVBQUUsQ0FBQztRQUVmLGFBQVEsR0FBRyxDQUFDLEtBQUssQ0FBQztRQUNsQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLFNBQUksR0FBRyxFQUFFLENBQUM7UUFDVixZQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ1osU0FBSSxHQUFHLENBQUMsQ0FBQztRQUNULFlBQU8sR0FBRyxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBR3BCLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBRW5CLFVBQUssR0FBRyxFQUFFLENBQUM7UUFFbEIsV0FBTSxHQUFHLDRzR0FvSlQsQ0FBQztJQU9FLENBQUM7SUFFSixvQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7SUFFRCxvQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVNLGtDQUFNLEdBQWI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELFlBQVk7SUFDWixzQ0FBVSxHQUFWLFVBQVcsS0FBSztRQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDdEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBRW5DLDRCQUE0QjtRQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyx1Q0FBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztRQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztRQUU5QixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELDhDQUFrQixHQUFsQixVQUFtQixJQUFJO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztRQUN6QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQyxPQUFPLENBQUMsR0FBRyxDQUNULDBCQUEwQjtZQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDdEIsU0FBUztZQUNULElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUN6QixJQUFJLENBQ0wsQ0FBQztJQUNKLENBQUM7SUFFRCx5Q0FBYSxHQUFiLFVBQWMsSUFBSTtRQUNoQixPQUFPLENBQUMsR0FBRyxDQUNULGlCQUFpQjtZQUNmLElBQUksQ0FBQyxTQUFTO1lBQ2Qsa0JBQWtCO1lBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSztZQUNqQixTQUFTO1lBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUTtZQUM3QixTQUFTO1lBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUNoQyxJQUFJLENBQ0wsQ0FBQztJQUNKLENBQUM7SUFFRCwyQ0FBZSxHQUFmLFVBQWdCLElBQUk7UUFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FDVCxrQkFBa0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FDaEQsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUNELG1EQUF1QixHQUF2QixVQUF3QixVQUFVO1FBQWxDLGlCQXNCQztRQXJCQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixJQUFNLFNBQVMsR0FDYiwrREFBK0QsQ0FBQztRQUVsRSxNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUNqQyxLQUFJLENBQUMsSUFBSTtpQkFDTixHQUFHLENBQ0MsU0FBUyxjQUFTLFNBQVMsQ0FDNUIsVUFBVSxDQUFDLEtBQUssQ0FDakIsK0RBQTRELENBQzlEO2lCQUNBLFNBQVMsQ0FBQyxVQUFBLEdBQUc7Z0JBQ1osMEJBQTBCO2dCQUMxQixPQUFPLENBQ0wsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBQSxLQUFLO29CQUM5QixLQUFLLENBQUMsV0FBVyxDQUFDO29CQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixDQUFDLENBQUMsQ0FDSCxDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwyREFBK0IsR0FBL0IsVUFBZ0MsVUFBVTtRQUN4QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRU0sZ0RBQW9CLEdBQTNCLFVBQTRCLElBQUk7UUFDOUIsSUFBSSxNQUFNLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUVqQywrQkFBK0I7SUFDakMsQ0FBQztJQW5SdUI7UUFBdkIsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7a0NBQVksaUJBQVU7d0RBQUM7SUFDNUI7UUFBakIsZ0JBQVMsQ0FBQyxLQUFLLENBQUM7a0NBQWdCLGlCQUFVOzREQUFDO0lBRmpDLGlCQUFpQjtRQUw3QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLDhCQUE4QjtZQUMzQyxTQUFTLEVBQUUsQ0FBQyxvQ0FBb0MsQ0FBQztTQUNsRCxDQUFDO3lDQTRLa0IsZUFBTTtZQUNSLFdBQUk7WUFDUSx5QkFBZ0I7WUFDNUIsV0FBSTtPQTlLVCxpQkFBaUIsQ0FxUjdCO0lBQUQsd0JBQUM7Q0FBQSxBQXJSRCxJQXFSQztBQXJSWSw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IE1hcFZpZXcsIE1hcmtlciwgUG9zaXRpb24sIFN0eWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1nb29nbGUtbWFwcy1zZGtcIjtcclxuaW1wb3J0IHsgSHR0cCB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCBcInJ4anMvUnhcIjtcclxuaW1wb3J0IHsgU2xpZGVyIH0gZnJvbSBcInVpL3NsaWRlclwiO1xyXG5cclxucmVnaXN0ZXJFbGVtZW50KFwiTWFwVmlldzJcIiwgKCkgPT4gTWFwVmlldyk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJteS1hcHBcIixcclxuICB0ZW1wbGF0ZVVybDogXCJwYWdlcy9jYW1wYWlnbi9jYW1wYWlnbi5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJwYWdlcy9jYW1wYWlnbi9jYW1wYWlnbi1jb21tb24uY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYW1wYWlnbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQFZpZXdDaGlsZChcImNvbnRhaW5lclwiKSBjb250YWluZXI6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcIkNCMVwiKSBGaXJzdENoZWNrQm94OiBFbGVtZW50UmVmO1xyXG5cclxuICBsYXN0Q2FtZXJhOiBTdHJpbmc7XHJcblxyXG4gIG1hcmtlciA9IG5ldyBNYXJrZXIoKTtcclxuXHJcbiAgcHVibGljIGxhdGl0dWRlID0gLTMzLjg2O1xyXG4gIHB1YmxpYyBsb25naXR1ZGUgPSAxNTEuMjtcclxuICB6b29tID0gMTQ7XHJcbiAgYmVhcmluZyA9IDA7XHJcbiAgdGlsdCA9IDA7XHJcbiAgcGFkZGluZyA9IFs0MCwgNDAsIDQwLCA0MF07XHJcbiAgbWFwVmlldzogTWFwVmlldztcclxuXHJcbiAgcHVibGljIHNlYXJjaFJlc3VsdHMgPSBbXTtcclxuICBwdWJsaWMgc3RhcnRMb2NhdGlvbkFkZHJlc3M7XHJcbiAgcHVibGljIHBsYWNlID0gXCJcIjtcclxuXHJcbiAgc3R5bGVzID0gYFtcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwiYWRtaW5pc3RyYXRpdmVcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib25cIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImxpZ2h0bmVzc1wiOiAzM1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwibGFuZHNjYXBlXCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFsbFwiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwiY29sb3JcIjogXCIjZjdmN2Y3XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInBvaS5idXNpbmVzc1wiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvZmZcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicG9pLnBhcmtcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZ2VvbWV0cnlcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiI2RlZWNkYlwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJwb2kucGFya1wiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJsYWJlbHNcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvblwiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwibGlnaHRuZXNzXCI6IFwiMjVcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicm9hZFwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImxpZ2h0bmVzc1wiOiBcIjI1XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInJvYWRcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwibGFiZWxzLmljb25cIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvZmZcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicm9hZC5oaWdod2F5XCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImdlb21ldHJ5XCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiNmZmZmZmZcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicm9hZC5oaWdod2F5XCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImxhYmVsc1wiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwic2F0dXJhdGlvblwiOiBcIi05MFwiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwibGlnaHRuZXNzXCI6IFwiMjVcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicm9hZC5hcnRlcmlhbFwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvblwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJyb2FkLmFydGVyaWFsXCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImdlb21ldHJ5XCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiNmZmZmZmZcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicm9hZC5sb2NhbFwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJnZW9tZXRyeVwiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwiY29sb3JcIjogXCIjZmZmZmZmXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInRyYW5zaXQubGluZVwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvZmZcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwidHJhbnNpdC5zdGF0aW9uXCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFsbFwiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwidmlzaWJpbGl0eVwiOiBcIm9mZlwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJ3YXRlclwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvblwiXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwiY29sb3JcIjogXCIjZTBmMWY5XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH1cclxuXWA7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgcGFnZTogUGFnZSxcclxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIHByaXZhdGUgaHR0cDogSHR0cFxyXG4gICkge31cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIG5leHRQYWdlKCkge1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2NhbXBhaWduZGV0YWlsc1wiXSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ29CYWNrKCkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2soKTtcclxuICB9XHJcblxyXG4gIC8vTWFwIGV2ZW50c1xyXG4gIG9uTWFwUmVhZHkoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiTWFwIFJlYWR5XCIpO1xyXG5cclxuICAgIHRoaXMubWFwVmlldyA9IGV2ZW50Lm9iamVjdDtcclxuICAgIHRoaXMubWFwVmlldy5zZXRTdHlsZSg8U3R5bGU+SlNPTi5wYXJzZSh0aGlzLnN0eWxlcykpO1xyXG4gICAgY29uc29sZS5sb2coXCJTZXR0aW5nIGEgbWFya2VyLi4uXCIpO1xyXG5cclxuICAgIC8vdmFyIG1hcmtlciA9IG5ldyBNYXJrZXIoKTtcclxuICAgIHRoaXMubWFya2VyLnBvc2l0aW9uID0gUG9zaXRpb24ucG9zaXRpb25Gcm9tTGF0TG5nKC0zMy44NiwgMTUxLjIpO1xyXG4gICAgdGhpcy5tYXJrZXIudGl0bGUgPSBcIlN5ZG5leVwiO1xyXG4gICAgdGhpcy5tYXJrZXIuZHJhZ2dhYmxlID0gdHJ1ZTtcclxuICAgIHRoaXMubWFya2VyLnNuaXBwZXQgPSBcIkF1c3RyYWxpYVwiO1xyXG4gICAgdGhpcy5tYXJrZXIudXNlckRhdGEgPSB7IGluZGV4OiAxIH07XHJcbiAgICB0aGlzLm1hcmtlci5pY29uID0gXCJ4MTAyaWNvblwiO1xyXG5cclxuICAgIHRoaXMubWFwVmlldy5hZGRNYXJrZXIodGhpcy5tYXJrZXIpO1xyXG4gIH1cclxuXHJcbiAgb25Db29yZGluYXRlVGFwcGVkKGFyZ3MpIHtcclxuICAgIHRoaXMubWFya2VyLnBvc2l0aW9uID0gYXJncy5wb3NpdGlvbjtcclxuICAgIHRoaXMubG9uZ2l0dWRlID0gYXJncy5wb3NpdGlvbi5sb25naXR1ZGU7XHJcbiAgICB0aGlzLmxhdGl0dWRlID0gYXJncy5wb3NpdGlvbi5sYXRpdHVkZTtcclxuICAgIHRoaXMubWFwVmlldy5hZGRNYXJrZXIodGhpcy5tYXJrZXIpO1xyXG4gICAgY29uc29sZS5sb2coXHJcbiAgICAgIFwiQ29vcmRpbmF0ZSBUYXBwZWQsIExhdDogXCIgK1xyXG4gICAgICAgIGFyZ3MucG9zaXRpb24ubGF0aXR1ZGUgK1xyXG4gICAgICAgIFwiLCBMb246IFwiICtcclxuICAgICAgICBhcmdzLnBvc2l0aW9uLmxvbmdpdHVkZSxcclxuICAgICAgYXJnc1xyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIG9uTWFya2VyRXZlbnQoYXJncykge1xyXG4gICAgY29uc29sZS5sb2coXHJcbiAgICAgIFwiTWFya2VyIEV2ZW50OiAnXCIgK1xyXG4gICAgICAgIGFyZ3MuZXZlbnROYW1lICtcclxuICAgICAgICBcIicgdHJpZ2dlcmVkIG9uOiBcIiArXHJcbiAgICAgICAgYXJncy5tYXJrZXIudGl0bGUgK1xyXG4gICAgICAgIFwiLCBMYXQ6IFwiICtcclxuICAgICAgICBhcmdzLm1hcmtlci5wb3NpdGlvbi5sYXRpdHVkZSArXHJcbiAgICAgICAgXCIsIExvbjogXCIgK1xyXG4gICAgICAgIGFyZ3MubWFya2VyLnBvc2l0aW9uLmxvbmdpdHVkZSxcclxuICAgICAgYXJnc1xyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIG9uQ2FtZXJhQ2hhbmdlZChhcmdzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcclxuICAgICAgXCJDYW1lcmEgY2hhbmdlZDogXCIgKyBKU09OLnN0cmluZ2lmeShhcmdzLmNhbWVyYSksXHJcbiAgICAgIEpTT04uc3RyaW5naWZ5KGFyZ3MuY2FtZXJhKSA9PT0gdGhpcy5sYXN0Q2FtZXJhXHJcbiAgICApO1xyXG4gICAgdGhpcy5sYXN0Q2FtZXJhID0gSlNPTi5zdHJpbmdpZnkoYXJncy5jYW1lcmEpO1xyXG4gIH1cclxuICBsb2NhdGlvblNlYXJjaEZyb21JbnB1dChzZWFyY2hXb3JkKSB7XHJcbiAgICBjb25zb2xlLmRpcihzZWFyY2hXb3JkLnZhbHVlKTtcclxuICAgIGNvbnN0IHBsYWNlc1VybCA9XHJcbiAgICAgIFwiaHR0cHM6Ly9tYXBzLmdvb2dsZWFwaXMuY29tL21hcHMvYXBpL3BsYWNlL2F1dG9jb21wbGV0ZS9qc29uP1wiO1xyXG5cclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIHRoaXMuaHR0cFxyXG4gICAgICAgIC5nZXQoXHJcbiAgICAgICAgICBgJHtwbGFjZXNVcmx9aW5wdXQ9JHtlbmNvZGVVUkkoXHJcbiAgICAgICAgICAgIHNlYXJjaFdvcmQudmFsdWVcclxuICAgICAgICAgICl9JnR5cGVzPWdlb2NvZGUma2V5PUFJemFTeUFHQUZUaW92aTFYR2xvZWVNVXd3VE85dlB6Z1VWaTNWRWBcclxuICAgICAgICApXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgLy9jb25zb2xlLmRpcihyZXMuanNvbigpKTtcclxuICAgICAgICAgIHJlc29sdmUoXHJcbiAgICAgICAgICAgIHJlcy5qc29uKCkucHJlZGljdGlvbnMubWFwKHBsYWNlID0+IHtcclxuICAgICAgICAgICAgICBwbGFjZS5kZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgICBjb25zb2xlLmRpcihwbGFjZSk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZWxlY3RQbGFjZUZyb21BdXRvY29tcGxldGVMaXN0KHBsYWNlSW5kZXgpIHtcclxuICAgIHRoaXMuc3RhcnRMb2NhdGlvbkFkZHJlc3MgPSB0aGlzLnNlYXJjaFJlc3VsdHNbcGxhY2VJbmRleF07XHJcbiAgICB0aGlzLnNlYXJjaFJlc3VsdHMgPSBbXTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblNlY29uZFNsaWRlckNoYW5nZShhcmdzKSB7XHJcbiAgICBsZXQgc2xpZGVyID0gPFNsaWRlcj5hcmdzLm9iamVjdDtcclxuXHJcbiAgICAvL3RoaXMuZm9udFNpemUgPSBzbGlkZXIudmFsdWU7XHJcbiAgfVxyXG59XHJcbiJdfQ==