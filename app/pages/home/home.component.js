"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("@angular/router");
var frame_1 = require("ui/frame");
var nativescript_angular_1 = require("nativescript-angular");
var nativescript_bottombar_1 = require("nativescript-bottombar");
var nativescript_google_maps_sdk_1 = require("nativescript-google-maps-sdk");
nativescript_angular_1.registerElement("BottomBar", function () { return nativescript_bottombar_1.BottomBar; });
// Important - must register MapView plugin in order to use in Angular templates
nativescript_angular_1.registerElement("MapView", function () { return nativescript_google_maps_sdk_1.MapView; });
var HomeComponent = (function () {
    function HomeComponent(router, page) {
        this.router = router;
        this.page = page;
        this.loggedIn = false;
        this.dashboard = false;
        this.profile = true;
        this.explore = true;
        this.campaings = true;
        this.latitude = -33.86;
        this.longitude = 151.2;
        this.zoom = 14;
        this.bearing = 0;
        this.tilt = 0;
        this.padding = [40, 40, 40, 40];
        this.styles = "[\n    {\n        \"featureType\": \"administrative\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            },\n            {\n                \"lightness\": 33\n            }\n        ]\n    },\n    {\n        \"featureType\": \"landscape\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"color\": \"#f7f7f7\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"poi.business\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"poi.park\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#deecdb\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"poi.park\",\n        \"elementType\": \"labels\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            },\n            {\n                \"lightness\": \"25\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"lightness\": \"25\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road\",\n        \"elementType\": \"labels.icon\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.highway\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#ffffff\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.highway\",\n        \"elementType\": \"labels\",\n        \"stylers\": [\n            {\n                \"saturation\": \"-90\"\n            },\n            {\n                \"lightness\": \"25\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.arterial\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.arterial\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#ffffff\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"road.local\",\n        \"elementType\": \"geometry\",\n        \"stylers\": [\n            {\n                \"color\": \"#ffffff\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"transit.line\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"transit.station\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"off\"\n            }\n        ]\n    },\n    {\n        \"featureType\": \"water\",\n        \"elementType\": \"all\",\n        \"stylers\": [\n            {\n                \"visibility\": \"on\"\n            },\n            {\n                \"color\": \"#e0f1f9\"\n            }\n        ]\n    }\n]";
        this.items = [
            new nativescript_bottombar_1.BottomBarItem(0, "Dashboard", "globewhite", "black"),
            new nativescript_bottombar_1.BottomBarItem(1, "Explore", "explorewhite", "black"),
            new nativescript_bottombar_1.BottomBarItem(2, "Camera", "camerawhite", "black"),
            new nativescript_bottombar_1.BottomBarItem(3, "Campaings", "campaignwhite", "black"),
            new nativescript_bottombar_1.BottomBarItem(4, "Profile", "userwhite", "black")
        ];
        this.dataItems = [
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            },
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            },
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            },
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            },
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            },
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            },
            {
                name: "DX3",
                price: "1000",
                location: "Toronto Convention Center",
                points: 600000,
                status: "Inactive",
                start: "Mar 8,2017",
                end: "Mar 17,2017"
            }
        ];
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundImage = "res://phonebg";
    };
    HomeComponent.prototype.addCampaign = function () {
        console.log("clicked");
        this.router.navigate(["/campaign"]);
    };
    HomeComponent.prototype.submit = function () {
        alert("Login with facebook to continue!");
    };
    HomeComponent.prototype.homePage = function () {
        console.log(this.FirstCheckBox.nativeElement.checked);
        if (this.FirstCheckBox.nativeElement.checked) {
            alert("You agreed");
        }
        else {
            alert("Agree with terms & conditions first!");
        }
    };
    HomeComponent.prototype.tabLoaded = function (event) {
        this._bar = event.object;
        this.hidden = false;
        this.titleState = 1 /* ALWAYS_SHOW */;
        this.inactiveColor = "white";
        this.accentColor = "#13acb7";
        this.uncoloredBackgroundColor = "#222";
    };
    HomeComponent.prototype.tabSelected = function (args) {
        // only triggered when a different tab is tapped
        console.log(args.oldIndex);
        if (args.newIndex == 0) {
            this.dashboard = false;
            this.explore = true;
            this.campaings = true;
            this.profile = true;
        }
        else if (args.newIndex == 1) {
            this.dashboard = true;
            this.explore = false;
            this.campaings = true;
            this.profile = true;
        }
        else if (args.newIndex == 3) {
            this.dashboard = true;
            this.explore = true;
            this.campaings = false;
            this.profile = true;
        }
        else if (args.newIndex == 4) {
            this.dashboard = true;
            this.explore = true;
            this.campaings = true;
            this.profile = false;
        }
        else {
            alert("Login with facebook to continue!");
            this._bar.selectItem(args.oldIndex);
        }
    };
    //Map events
    HomeComponent.prototype.onMapReady = function (event) {
        console.log("Map Ready");
        this.mapView = event.object;
        this.mapView.setStyle(JSON.parse(this.styles));
        console.log("Setting a marker...");
        var marker = new nativescript_google_maps_sdk_1.Marker();
        marker.position = nativescript_google_maps_sdk_1.Position.positionFromLatLng(-33.86, 151.2);
        marker.title = "Sydney";
        marker.snippet = "Australia";
        marker.userData = { index: 1 };
        marker.icon = "x102icon";
        console.dir(marker);
        this.mapView.addMarker(marker);
    };
    HomeComponent.prototype.onCoordinateTapped = function (args) {
        console.log("Coordinate Tapped, Lat: " +
            args.position.latitude +
            ", Lon: " +
            args.position.longitude, args);
    };
    HomeComponent.prototype.onMarkerEvent = function (args) {
        console.log("Marker Event: '" +
            args.eventName +
            "' triggered on: " +
            args.marker.title +
            ", Lat: " +
            args.marker.position.latitude +
            ", Lon: " +
            args.marker.position.longitude, args);
    };
    HomeComponent.prototype.onCameraChanged = function (args) {
        console.log("Camera changed: " + JSON.stringify(args.camera), JSON.stringify(args.camera) === this.lastCamera);
        this.lastCamera = JSON.stringify(args.camera);
    };
    HomeComponent.prototype.logIn = function () {
        this.loggedIn = !this.loggedIn;
    };
    HomeComponent.prototype.onCellSwiping = function (args) {
        var swipeLimits = args.data.swipeLimits;
        var currentItemView = args.object;
        var currentView;
        if (args.data.x > 200) {
            console.log("Notify perform left action");
        }
        else if (args.data.x < -200) {
            console.log("Notify perform right action");
        }
    };
    HomeComponent.prototype.onSwipeCellStarted = function (args) {
        var swipeLimits = args.data.swipeLimits;
        var listView = frame_1.topmost().currentPage.getViewById("listView");
        swipeLimits.threshold = listView.getMeasuredWidth();
        swipeLimits.left = listView.getMeasuredWidth();
        swipeLimits.right = listView.getMeasuredWidth();
    };
    HomeComponent.prototype.onSwipeCellFinished = function (args) {
        if (args.data.x > 200) {
            console.log("Perform left action");
        }
        else if (args.data.x < -200) {
            console.log("Perform right action");
        }
    };
    HomeComponent.prototype.onItemClick = function (args) {
        var listView = frame_1.topmost().currentPage.getViewById("listView");
        listView.notifySwipeToExecuteFinished();
        console.log("Item click: " + args.index);
    };
    HomeComponent.prototype.onLeftSwipeClick = function (args) {
        console.log("Left swipe click");
    };
    HomeComponent.prototype.onRightSwipeClick = function (args) {
        console.log("Right swipe click");
    };
    __decorate([
        core_1.ViewChild("container"),
        __metadata("design:type", core_1.ElementRef)
    ], HomeComponent.prototype, "container", void 0);
    __decorate([
        core_1.ViewChild("CB1"),
        __metadata("design:type", core_1.ElementRef)
    ], HomeComponent.prototype, "FirstCheckBox", void 0);
    HomeComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "pages/home/home.html",
            styleUrls: ["pages/home/home.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router, page_1.Page])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUN6RSxnQ0FBK0I7QUFDL0IsMENBQXlDO0FBRXpDLGtDQUFtQztBQUNuQyw2REFBdUQ7QUFDdkQsaUVBTWdDO0FBQ2hDLDZFQUFnRjtBQU9oRixzQ0FBZSxDQUFDLFdBQVcsRUFBRSxjQUFNLE9BQUEsa0NBQVMsRUFBVCxDQUFTLENBQUMsQ0FBQztBQUU5QyxnRkFBZ0Y7QUFDaEYsc0NBQWUsQ0FBQyxTQUFTLEVBQUUsY0FBTSxPQUFBLHNDQUFPLEVBQVAsQ0FBTyxDQUFDLENBQUM7QUFPMUM7SUE0UEUsdUJBQW9CLE1BQWMsRUFBVSxJQUFVO1FBQWxDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBdlAvQyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBUTFCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTyxHQUFZLElBQUksQ0FBQztRQUN4QixZQUFPLEdBQVksSUFBSSxDQUFDO1FBQ3hCLGNBQVMsR0FBWSxJQUFJLENBQUM7UUFFakMsYUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsU0FBSSxHQUFHLEVBQUUsQ0FBQztRQUNWLFlBQU8sR0FBRyxDQUFDLENBQUM7UUFDWixTQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ1QsWUFBTyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFHM0IsV0FBTSxHQUFHLDRzR0FvSlQsQ0FBQztRQUlNLFVBQUssR0FBeUI7WUFDbkMsSUFBSSxzQ0FBYSxDQUFDLENBQUMsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLE9BQU8sQ0FBQztZQUN4RCxJQUFJLHNDQUFhLENBQUMsQ0FBQyxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsT0FBTyxDQUFDO1lBQ3hELElBQUksc0NBQWEsQ0FBQyxDQUFDLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxPQUFPLENBQUM7WUFDdEQsSUFBSSxzQ0FBYSxDQUFDLENBQUMsRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUFFLE9BQU8sQ0FBQztZQUMzRCxJQUFJLHNDQUFhLENBQUMsQ0FBQyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsT0FBTyxDQUFDO1NBQ3RELENBQUM7UUFFSyxjQUFTLEdBQUc7WUFDakI7Z0JBQ0UsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsUUFBUSxFQUFFLDJCQUEyQjtnQkFDckMsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsTUFBTSxFQUFFLFVBQVU7Z0JBQ2xCLEtBQUssRUFBRSxZQUFZO2dCQUNuQixHQUFHLEVBQUUsYUFBYTthQUNuQjtZQUNEO2dCQUNFLElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssRUFBRSxNQUFNO2dCQUNiLFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLE1BQU0sRUFBRSxVQUFVO2dCQUNsQixLQUFLLEVBQUUsWUFBWTtnQkFDbkIsR0FBRyxFQUFFLGFBQWE7YUFDbkI7WUFDRDtnQkFDRSxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLEVBQUUsTUFBTTtnQkFDYixRQUFRLEVBQUUsMkJBQTJCO2dCQUNyQyxNQUFNLEVBQUUsTUFBTTtnQkFDZCxNQUFNLEVBQUUsVUFBVTtnQkFDbEIsS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLEdBQUcsRUFBRSxhQUFhO2FBQ25CO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsUUFBUSxFQUFFLDJCQUEyQjtnQkFDckMsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsTUFBTSxFQUFFLFVBQVU7Z0JBQ2xCLEtBQUssRUFBRSxZQUFZO2dCQUNuQixHQUFHLEVBQUUsYUFBYTthQUNuQjtZQUNEO2dCQUNFLElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssRUFBRSxNQUFNO2dCQUNiLFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLE1BQU0sRUFBRSxVQUFVO2dCQUNsQixLQUFLLEVBQUUsWUFBWTtnQkFDbkIsR0FBRyxFQUFFLGFBQWE7YUFDbkI7WUFDRDtnQkFDRSxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLEVBQUUsTUFBTTtnQkFDYixRQUFRLEVBQUUsMkJBQTJCO2dCQUNyQyxNQUFNLEVBQUUsTUFBTTtnQkFDZCxNQUFNLEVBQUUsVUFBVTtnQkFDbEIsS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLEdBQUcsRUFBRSxhQUFhO2FBQ25CO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsUUFBUSxFQUFFLDJCQUEyQjtnQkFDckMsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsTUFBTSxFQUFFLFVBQVU7Z0JBQ2xCLEtBQUssRUFBRSxZQUFZO2dCQUNuQixHQUFHLEVBQUUsYUFBYTthQUNuQjtTQUNGLENBQUM7SUFFdUQsQ0FBQztJQUUxRCxnQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztJQUM5QyxDQUFDO0lBRUQsbUNBQVcsR0FBWDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFRCw4QkFBTSxHQUFOO1FBQ0UsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3RELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDN0MsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3RCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO1FBQ2hELENBQUM7SUFDSCxDQUFDO0lBRUQsaUNBQVMsR0FBVCxVQUFVLEtBQUs7UUFDYixJQUFJLENBQUMsSUFBSSxHQUFjLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFVBQVUsc0JBQTBCLENBQUM7UUFDMUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUM7UUFDN0IsSUFBSSxDQUFDLHdCQUF3QixHQUFHLE1BQU0sQ0FBQztJQUN6QyxDQUFDO0lBRUQsbUNBQVcsR0FBWCxVQUFZLElBQW1DO1FBQzdDLGdEQUFnRDtRQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDdEIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDdEIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDdEIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7SUFDSCxDQUFDO0lBRUQsWUFBWTtJQUNaLGtDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUV6QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFFbkMsSUFBSSxNQUFNLEdBQUcsSUFBSSxxQ0FBTSxFQUFFLENBQUM7UUFDMUIsTUFBTSxDQUFDLFFBQVEsR0FBRyx1Q0FBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdELE1BQU0sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDL0IsTUFBTSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsMENBQWtCLEdBQWxCLFVBQW1CLElBQUk7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FDVCwwQkFBMEI7WUFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ3RCLFNBQVM7WUFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFDekIsSUFBSSxDQUNMLENBQUM7SUFDSixDQUFDO0lBRUQscUNBQWEsR0FBYixVQUFjLElBQUk7UUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FDVCxpQkFBaUI7WUFDZixJQUFJLENBQUMsU0FBUztZQUNkLGtCQUFrQjtZQUNsQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDakIsU0FBUztZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDN0IsU0FBUztZQUNULElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFDaEMsSUFBSSxDQUNMLENBQUM7SUFDSixDQUFDO0lBRUQsdUNBQWUsR0FBZixVQUFnQixJQUFJO1FBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQ1Qsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQ2hELENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw2QkFBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDakMsQ0FBQztJQUNNLHFDQUFhLEdBQXBCLFVBQXFCLElBQTJCO1FBQzlDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ3hDLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDbEMsSUFBSSxXQUFXLENBQUM7UUFFaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1FBQzdDLENBQUM7SUFDSCxDQUFDO0lBRU0sMENBQWtCLEdBQXpCLFVBQTBCLElBQTJCO1FBQ25ELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ3hDLElBQUksUUFBUSxHQUFHLGVBQU8sRUFBRSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFTLENBQUM7UUFFckUsV0FBVyxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUNwRCxXQUFXLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQy9DLFdBQVcsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDbEQsQ0FBQztJQUVNLDJDQUFtQixHQUExQixVQUEyQixJQUEyQjtRQUNwRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDdEMsQ0FBQztJQUNILENBQUM7SUFFTSxtQ0FBVyxHQUFsQixVQUFtQixJQUF1QjtRQUN4QyxJQUFJLFFBQVEsR0FBZ0IsZUFBTyxFQUFFLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxRSxRQUFRLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLHdDQUFnQixHQUF2QixVQUF3QixJQUFJO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRU0seUNBQWlCLEdBQXhCLFVBQXlCLElBQUk7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUF4WnVCO1FBQXZCLGdCQUFTLENBQUMsV0FBVyxDQUFDO2tDQUFZLGlCQUFVO29EQUFDO0lBQzVCO1FBQWpCLGdCQUFTLENBQUMsS0FBSyxDQUFDO2tDQUFnQixpQkFBVTt3REFBQztJQUZqQyxhQUFhO1FBTHpCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsc0JBQXNCO1lBQ25DLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1NBQ25DLENBQUM7eUNBNlA0QixlQUFNLEVBQWdCLFdBQUk7T0E1UDNDLGFBQWEsQ0EwWnpCO0lBQUQsb0JBQUM7Q0FBQSxBQTFaRCxJQTBaQztBQTFaWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBWaWV3IH0gZnJvbSBcInVpL2NvcmUvdmlld1wiO1xyXG5pbXBvcnQgeyB0b3Btb3N0IH0gZnJvbSBcInVpL2ZyYW1lXCI7XHJcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhclwiO1xyXG5pbXBvcnQge1xyXG4gIEJvdHRvbUJhcixcclxuICBCb3R0b21CYXJJdGVtLFxyXG4gIFRJVExFX1NUQVRFLFxyXG4gIFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhLFxyXG4gIE5vdGlmaWNhdGlvblxyXG59IGZyb20gXCJuYXRpdmVzY3JpcHQtYm90dG9tYmFyXCI7XHJcbmltcG9ydCB7IE1hcFZpZXcsIE1hcmtlciwgUG9zaXRpb24sIFN0eWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1nb29nbGUtbWFwcy1zZGtcIjtcclxuaW1wb3J0IHtcclxuICBSYWRMaXN0VmlldyxcclxuICBMaXN0Vmlld0V2ZW50RGF0YSxcclxuICBTd2lwZUFjdGlvbnNFdmVudERhdGFcclxufSBmcm9tIFwibmF0aXZlc2NyaXB0LXRlbGVyaWstdWkvbGlzdHZpZXdcIjtcclxuXHJcbnJlZ2lzdGVyRWxlbWVudChcIkJvdHRvbUJhclwiLCAoKSA9PiBCb3R0b21CYXIpO1xyXG5cclxuLy8gSW1wb3J0YW50IC0gbXVzdCByZWdpc3RlciBNYXBWaWV3IHBsdWdpbiBpbiBvcmRlciB0byB1c2UgaW4gQW5ndWxhciB0ZW1wbGF0ZXNcclxucmVnaXN0ZXJFbGVtZW50KFwiTWFwVmlld1wiLCAoKSA9PiBNYXBWaWV3KTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm15LWFwcFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcInBhZ2VzL2hvbWUvaG9tZS5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJwYWdlcy9ob21lL2hvbWUuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBAVmlld0NoaWxkKFwiY29udGFpbmVyXCIpIGNvbnRhaW5lcjogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwiQ0IxXCIpIEZpcnN0Q2hlY2tCb3g6IEVsZW1lbnRSZWY7XHJcblxyXG4gIHB1YmxpYyBoaWRkZW46IGJvb2xlYW47XHJcbiAgcHVibGljIGxvZ2dlZEluOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gIHB1YmxpYyB0aXRsZVN0YXRlOiBUSVRMRV9TVEFURTtcclxuICBwdWJsaWMgX2JhcjogQm90dG9tQmFyO1xyXG4gIHB1YmxpYyBpbmFjdGl2ZUNvbG9yOiBzdHJpbmc7XHJcbiAgcHVibGljIGFjY2VudENvbG9yOiBzdHJpbmc7XHJcbiAgcHVibGljIHVuY29sb3JlZEJhY2tncm91bmRDb2xvcjogc3RyaW5nO1xyXG5cclxuICBwdWJsaWMgZGFzaGJvYXJkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIHByb2ZpbGU6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIHB1YmxpYyBleHBsb3JlOiBib29sZWFuID0gdHJ1ZTtcclxuICBwdWJsaWMgY2FtcGFpbmdzOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgbGF0aXR1ZGUgPSAtMzMuODY7XHJcbiAgbG9uZ2l0dWRlID0gMTUxLjI7XHJcbiAgem9vbSA9IDE0O1xyXG4gIGJlYXJpbmcgPSAwO1xyXG4gIHRpbHQgPSAwO1xyXG4gIHBhZGRpbmcgPSBbNDAsIDQwLCA0MCwgNDBdO1xyXG4gIG1hcFZpZXc6IE1hcFZpZXc7XHJcblxyXG4gIHN0eWxlcyA9IGBbXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcImFkbWluaXN0cmF0aXZlXCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFsbFwiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwidmlzaWJpbGl0eVwiOiBcIm9uXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJsaWdodG5lc3NcIjogMzNcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcImxhbmRzY2FwZVwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiI2Y3ZjdmN1wiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJwb2kuYnVzaW5lc3NcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib2ZmXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInBvaS5wYXJrXCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImdlb21ldHJ5XCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiNkZWVjZGJcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicG9pLnBhcmtcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwibGFiZWxzXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib25cIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImxpZ2h0bmVzc1wiOiBcIjI1XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInJvYWRcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJsaWdodG5lc3NcIjogXCIyNVwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJyb2FkXCIsXHJcbiAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImxhYmVscy5pY29uXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib2ZmXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInJvYWQuaGlnaHdheVwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJnZW9tZXRyeVwiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwiY29sb3JcIjogXCIjZmZmZmZmXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInJvYWQuaGlnaHdheVwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJsYWJlbHNcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInNhdHVyYXRpb25cIjogXCItOTBcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImxpZ2h0bmVzc1wiOiBcIjI1XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInJvYWQuYXJ0ZXJpYWxcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib25cIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicm9hZC5hcnRlcmlhbFwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJnZW9tZXRyeVwiLFxyXG4gICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwiY29sb3JcIjogXCIjZmZmZmZmXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInJvYWQubG9jYWxcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiZ2VvbWV0cnlcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiI2ZmZmZmZlwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJ0cmFuc2l0LmxpbmVcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib2ZmXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcInRyYW5zaXQuc3RhdGlvblwiLFxyXG4gICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJhbGxcIixcclxuICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInZpc2liaWxpdHlcIjogXCJvZmZcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwid2F0ZXJcIixcclxuICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib25cIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiI2UwZjFmOVwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9XHJcbl1gO1xyXG5cclxuICBsYXN0Q2FtZXJhOiBTdHJpbmc7XHJcblxyXG4gIHB1YmxpYyBpdGVtczogQXJyYXk8Qm90dG9tQmFySXRlbT4gPSBbXHJcbiAgICBuZXcgQm90dG9tQmFySXRlbSgwLCBcIkRhc2hib2FyZFwiLCBcImdsb2Jld2hpdGVcIiwgXCJibGFja1wiKSxcclxuICAgIG5ldyBCb3R0b21CYXJJdGVtKDEsIFwiRXhwbG9yZVwiLCBcImV4cGxvcmV3aGl0ZVwiLCBcImJsYWNrXCIpLFxyXG4gICAgbmV3IEJvdHRvbUJhckl0ZW0oMiwgXCJDYW1lcmFcIiwgXCJjYW1lcmF3aGl0ZVwiLCBcImJsYWNrXCIpLFxyXG4gICAgbmV3IEJvdHRvbUJhckl0ZW0oMywgXCJDYW1wYWluZ3NcIiwgXCJjYW1wYWlnbndoaXRlXCIsIFwiYmxhY2tcIiksXHJcbiAgICBuZXcgQm90dG9tQmFySXRlbSg0LCBcIlByb2ZpbGVcIiwgXCJ1c2Vyd2hpdGVcIiwgXCJibGFja1wiKVxyXG4gIF07XHJcblxyXG4gIHB1YmxpYyBkYXRhSXRlbXMgPSBbXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6IFwiRFgzXCIsXHJcbiAgICAgIHByaWNlOiBcIjEwMDBcIixcclxuICAgICAgbG9jYXRpb246IFwiVG9yb250byBDb252ZW50aW9uIENlbnRlclwiLFxyXG4gICAgICBwb2ludHM6IDYwMDAwMCxcclxuICAgICAgc3RhdHVzOiBcIkluYWN0aXZlXCIsXHJcbiAgICAgIHN0YXJ0OiBcIk1hciA4LDIwMTdcIixcclxuICAgICAgZW5kOiBcIk1hciAxNywyMDE3XCJcclxuICAgIH1cclxuICBdO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcbiAgICB0aGlzLnBhZ2UuYmFja2dyb3VuZEltYWdlID0gXCJyZXM6Ly9waG9uZWJnXCI7XHJcbiAgfVxyXG5cclxuICBhZGRDYW1wYWlnbigpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiY2xpY2tlZFwiKTtcclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9jYW1wYWlnblwiXSk7XHJcbiAgfVxyXG5cclxuICBzdWJtaXQoKSB7XHJcbiAgICBhbGVydChcIkxvZ2luIHdpdGggZmFjZWJvb2sgdG8gY29udGludWUhXCIpO1xyXG4gIH1cclxuXHJcbiAgaG9tZVBhZ2UoKSB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLkZpcnN0Q2hlY2tCb3gubmF0aXZlRWxlbWVudC5jaGVja2VkKTtcclxuICAgIGlmICh0aGlzLkZpcnN0Q2hlY2tCb3gubmF0aXZlRWxlbWVudC5jaGVja2VkKSB7XHJcbiAgICAgIGFsZXJ0KFwiWW91IGFncmVlZFwiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGFsZXJ0KFwiQWdyZWUgd2l0aCB0ZXJtcyAmIGNvbmRpdGlvbnMgZmlyc3QhXCIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdGFiTG9hZGVkKGV2ZW50KSB7XHJcbiAgICB0aGlzLl9iYXIgPSA8Qm90dG9tQmFyPmV2ZW50Lm9iamVjdDtcclxuICAgIHRoaXMuaGlkZGVuID0gZmFsc2U7XHJcbiAgICB0aGlzLnRpdGxlU3RhdGUgPSBUSVRMRV9TVEFURS5BTFdBWVNfU0hPVztcclxuICAgIHRoaXMuaW5hY3RpdmVDb2xvciA9IFwid2hpdGVcIjtcclxuICAgIHRoaXMuYWNjZW50Q29sb3IgPSBcIiMxM2FjYjdcIjtcclxuICAgIHRoaXMudW5jb2xvcmVkQmFja2dyb3VuZENvbG9yID0gXCIjMjIyXCI7XHJcbiAgfVxyXG5cclxuICB0YWJTZWxlY3RlZChhcmdzOiBTZWxlY3RlZEluZGV4Q2hhbmdlZEV2ZW50RGF0YSkge1xyXG4gICAgLy8gb25seSB0cmlnZ2VyZWQgd2hlbiBhIGRpZmZlcmVudCB0YWIgaXMgdGFwcGVkXHJcbiAgICBjb25zb2xlLmxvZyhhcmdzLm9sZEluZGV4KTtcclxuICAgIGlmIChhcmdzLm5ld0luZGV4ID09IDApIHtcclxuICAgICAgdGhpcy5kYXNoYm9hcmQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5leHBsb3JlID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jYW1wYWluZ3MgPSB0cnVlO1xyXG4gICAgICB0aGlzLnByb2ZpbGUgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmIChhcmdzLm5ld0luZGV4ID09IDEpIHtcclxuICAgICAgdGhpcy5kYXNoYm9hcmQgPSB0cnVlO1xyXG4gICAgICB0aGlzLmV4cGxvcmUgPSBmYWxzZTtcclxuICAgICAgdGhpcy5jYW1wYWluZ3MgPSB0cnVlO1xyXG4gICAgICB0aGlzLnByb2ZpbGUgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmIChhcmdzLm5ld0luZGV4ID09IDMpIHtcclxuICAgICAgdGhpcy5kYXNoYm9hcmQgPSB0cnVlO1xyXG4gICAgICB0aGlzLmV4cGxvcmUgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNhbXBhaW5ncyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLnByb2ZpbGUgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmIChhcmdzLm5ld0luZGV4ID09IDQpIHtcclxuICAgICAgdGhpcy5kYXNoYm9hcmQgPSB0cnVlO1xyXG4gICAgICB0aGlzLmV4cGxvcmUgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNhbXBhaW5ncyA9IHRydWU7XHJcbiAgICAgIHRoaXMucHJvZmlsZSA9IGZhbHNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgYWxlcnQoXCJMb2dpbiB3aXRoIGZhY2Vib29rIHRvIGNvbnRpbnVlIVwiKTtcclxuICAgICAgdGhpcy5fYmFyLnNlbGVjdEl0ZW0oYXJncy5vbGRJbmRleCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvL01hcCBldmVudHNcclxuICBvbk1hcFJlYWR5KGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIk1hcCBSZWFkeVwiKTtcclxuXHJcbiAgICB0aGlzLm1hcFZpZXcgPSBldmVudC5vYmplY3Q7XHJcbiAgICB0aGlzLm1hcFZpZXcuc2V0U3R5bGUoPFN0eWxlPkpTT04ucGFyc2UodGhpcy5zdHlsZXMpKTtcclxuICAgIGNvbnNvbGUubG9nKFwiU2V0dGluZyBhIG1hcmtlci4uLlwiKTtcclxuXHJcbiAgICB2YXIgbWFya2VyID0gbmV3IE1hcmtlcigpO1xyXG4gICAgbWFya2VyLnBvc2l0aW9uID0gUG9zaXRpb24ucG9zaXRpb25Gcm9tTGF0TG5nKC0zMy44NiwgMTUxLjIpO1xyXG4gICAgbWFya2VyLnRpdGxlID0gXCJTeWRuZXlcIjtcclxuICAgIG1hcmtlci5zbmlwcGV0ID0gXCJBdXN0cmFsaWFcIjtcclxuICAgIG1hcmtlci51c2VyRGF0YSA9IHsgaW5kZXg6IDEgfTtcclxuICAgIG1hcmtlci5pY29uID0gXCJ4MTAyaWNvblwiO1xyXG4gICAgY29uc29sZS5kaXIobWFya2VyKTtcclxuICAgIHRoaXMubWFwVmlldy5hZGRNYXJrZXIobWFya2VyKTtcclxuICB9XHJcblxyXG4gIG9uQ29vcmRpbmF0ZVRhcHBlZChhcmdzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcclxuICAgICAgXCJDb29yZGluYXRlIFRhcHBlZCwgTGF0OiBcIiArXHJcbiAgICAgICAgYXJncy5wb3NpdGlvbi5sYXRpdHVkZSArXHJcbiAgICAgICAgXCIsIExvbjogXCIgK1xyXG4gICAgICAgIGFyZ3MucG9zaXRpb24ubG9uZ2l0dWRlLFxyXG4gICAgICBhcmdzXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgb25NYXJrZXJFdmVudChhcmdzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcclxuICAgICAgXCJNYXJrZXIgRXZlbnQ6ICdcIiArXHJcbiAgICAgICAgYXJncy5ldmVudE5hbWUgK1xyXG4gICAgICAgIFwiJyB0cmlnZ2VyZWQgb246IFwiICtcclxuICAgICAgICBhcmdzLm1hcmtlci50aXRsZSArXHJcbiAgICAgICAgXCIsIExhdDogXCIgK1xyXG4gICAgICAgIGFyZ3MubWFya2VyLnBvc2l0aW9uLmxhdGl0dWRlICtcclxuICAgICAgICBcIiwgTG9uOiBcIiArXHJcbiAgICAgICAgYXJncy5tYXJrZXIucG9zaXRpb24ubG9uZ2l0dWRlLFxyXG4gICAgICBhcmdzXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgb25DYW1lcmFDaGFuZ2VkKGFyZ3MpIHtcclxuICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICBcIkNhbWVyYSBjaGFuZ2VkOiBcIiArIEpTT04uc3RyaW5naWZ5KGFyZ3MuY2FtZXJhKSxcclxuICAgICAgSlNPTi5zdHJpbmdpZnkoYXJncy5jYW1lcmEpID09PSB0aGlzLmxhc3RDYW1lcmFcclxuICAgICk7XHJcbiAgICB0aGlzLmxhc3RDYW1lcmEgPSBKU09OLnN0cmluZ2lmeShhcmdzLmNhbWVyYSk7XHJcbiAgfVxyXG5cclxuICBsb2dJbigpIHtcclxuICAgIHRoaXMubG9nZ2VkSW4gPSAhdGhpcy5sb2dnZWRJbjtcclxuICB9XHJcbiAgcHVibGljIG9uQ2VsbFN3aXBpbmcoYXJnczogU3dpcGVBY3Rpb25zRXZlbnREYXRhKSB7XHJcbiAgICB2YXIgc3dpcGVMaW1pdHMgPSBhcmdzLmRhdGEuc3dpcGVMaW1pdHM7XHJcbiAgICB2YXIgY3VycmVudEl0ZW1WaWV3ID0gYXJncy5vYmplY3Q7XHJcbiAgICB2YXIgY3VycmVudFZpZXc7XHJcblxyXG4gICAgaWYgKGFyZ3MuZGF0YS54ID4gMjAwKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiTm90aWZ5IHBlcmZvcm0gbGVmdCBhY3Rpb25cIik7XHJcbiAgICB9IGVsc2UgaWYgKGFyZ3MuZGF0YS54IDwgLTIwMCkge1xyXG4gICAgICBjb25zb2xlLmxvZyhcIk5vdGlmeSBwZXJmb3JtIHJpZ2h0IGFjdGlvblwiKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblN3aXBlQ2VsbFN0YXJ0ZWQoYXJnczogU3dpcGVBY3Rpb25zRXZlbnREYXRhKSB7XHJcbiAgICB2YXIgc3dpcGVMaW1pdHMgPSBhcmdzLmRhdGEuc3dpcGVMaW1pdHM7XHJcbiAgICB2YXIgbGlzdFZpZXcgPSB0b3Btb3N0KCkuY3VycmVudFBhZ2UuZ2V0Vmlld0J5SWQoXCJsaXN0Vmlld1wiKSBhcyBWaWV3O1xyXG5cclxuICAgIHN3aXBlTGltaXRzLnRocmVzaG9sZCA9IGxpc3RWaWV3LmdldE1lYXN1cmVkV2lkdGgoKTtcclxuICAgIHN3aXBlTGltaXRzLmxlZnQgPSBsaXN0Vmlldy5nZXRNZWFzdXJlZFdpZHRoKCk7XHJcbiAgICBzd2lwZUxpbWl0cy5yaWdodCA9IGxpc3RWaWV3LmdldE1lYXN1cmVkV2lkdGgoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblN3aXBlQ2VsbEZpbmlzaGVkKGFyZ3M6IFN3aXBlQWN0aW9uc0V2ZW50RGF0YSkge1xyXG4gICAgaWYgKGFyZ3MuZGF0YS54ID4gMjAwKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiUGVyZm9ybSBsZWZ0IGFjdGlvblwiKTtcclxuICAgIH0gZWxzZSBpZiAoYXJncy5kYXRhLnggPCAtMjAwKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiUGVyZm9ybSByaWdodCBhY3Rpb25cIik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25JdGVtQ2xpY2soYXJnczogTGlzdFZpZXdFdmVudERhdGEpIHtcclxuICAgIHZhciBsaXN0VmlldyA9IDxSYWRMaXN0Vmlldz50b3Btb3N0KCkuY3VycmVudFBhZ2UuZ2V0Vmlld0J5SWQoXCJsaXN0Vmlld1wiKTtcclxuICAgIGxpc3RWaWV3Lm5vdGlmeVN3aXBlVG9FeGVjdXRlRmluaXNoZWQoKTtcclxuICAgIGNvbnNvbGUubG9nKFwiSXRlbSBjbGljazogXCIgKyBhcmdzLmluZGV4KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkxlZnRTd2lwZUNsaWNrKGFyZ3MpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiTGVmdCBzd2lwZSBjbGlja1wiKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblJpZ2h0U3dpcGVDbGljayhhcmdzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIlJpZ2h0IHN3aXBlIGNsaWNrXCIpO1xyXG4gIH1cclxufVxyXG4iXX0=