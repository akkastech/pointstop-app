import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Page } from "ui/page";
import { Router } from "@angular/router";
import { View } from "ui/core/view";
import { topmost } from "ui/frame";
import { registerElement } from "nativescript-angular";
import {
  BottomBar,
  BottomBarItem,
  TITLE_STATE,
  SelectedIndexChangedEventData,
  Notification
} from "nativescript-bottombar";
import { MapView, Marker, Position, Style } from "nativescript-google-maps-sdk";
import {
  RadListView,
  ListViewEventData,
  SwipeActionsEventData
} from "nativescript-telerik-ui/listview";

registerElement("BottomBar", () => BottomBar);

// Important - must register MapView plugin in order to use in Angular templates
registerElement("MapView", () => MapView);

@Component({
  selector: "my-app",
  templateUrl: "pages/home/home.html",
  styleUrls: ["pages/home/home.css"]
})
export class HomeComponent implements OnInit {
  @ViewChild("container") container: ElementRef;
  @ViewChild("CB1") FirstCheckBox: ElementRef;

  public hidden: boolean;
  public loggedIn: boolean = false;

  public titleState: TITLE_STATE;
  public _bar: BottomBar;
  public inactiveColor: string;
  public accentColor: string;
  public uncoloredBackgroundColor: string;

  public dashboard: boolean = false;
  public profile: boolean = true;
  public explore: boolean = true;
  public campaings: boolean = true;

  latitude = -33.86;
  longitude = 151.2;
  zoom = 14;
  bearing = 0;
  tilt = 0;
  padding = [40, 40, 40, 40];
  mapView: MapView;

  styles = `[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 33
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f7f7f7"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#deecdb"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": "25"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "25"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "saturation": "-90"
            },
            {
                "lightness": "25"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e0f1f9"
            }
        ]
    }
]`;

  lastCamera: String;

  public items: Array<BottomBarItem> = [
    new BottomBarItem(0, "Dashboard", "globewhite", "black"),
    new BottomBarItem(1, "Explore", "explorewhite", "black"),
    new BottomBarItem(2, "Camera", "camerawhite", "black"),
    new BottomBarItem(3, "Campaings", "campaignwhite", "black"),
    new BottomBarItem(4, "Profile", "userwhite", "black")
  ];

  public dataItems = [
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    },
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    },
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    },
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    },
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    },
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    },
    {
      name: "DX3",
      price: "1000",
      location: "Toronto Convention Center",
      points: 600000,
      status: "Inactive",
      start: "Mar 8,2017",
      end: "Mar 17,2017"
    }
  ];

  constructor(private router: Router, private page: Page) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.page.backgroundImage = "res://phonebg";
  }

  addCampaign() {
    console.log("clicked");
    this.router.navigate(["/campaign"]);
  }

  submit() {
    alert("Login with facebook to continue!");
  }

  homePage() {
    console.log(this.FirstCheckBox.nativeElement.checked);
    if (this.FirstCheckBox.nativeElement.checked) {
      alert("You agreed");
    } else {
      alert("Agree with terms & conditions first!");
    }
  }

  tabLoaded(event) {
    this._bar = <BottomBar>event.object;
    this.hidden = false;
    this.titleState = TITLE_STATE.ALWAYS_SHOW;
    this.inactiveColor = "white";
    this.accentColor = "#13acb7";
    this.uncoloredBackgroundColor = "#222";
  }

  tabSelected(args: SelectedIndexChangedEventData) {
    // only triggered when a different tab is tapped
    console.log(args.oldIndex);
    if (args.newIndex == 0) {
      this.dashboard = false;
      this.explore = true;
      this.campaings = true;
      this.profile = true;
    } else if (args.newIndex == 1) {
      this.dashboard = true;
      this.explore = false;
      this.campaings = true;
      this.profile = true;
    } else if (args.newIndex == 3) {
      this.dashboard = true;
      this.explore = true;
      this.campaings = false;
      this.profile = true;
    } else if (args.newIndex == 4) {
      this.dashboard = true;
      this.explore = true;
      this.campaings = true;
      this.profile = false;
    } else {
      alert("Login with facebook to continue!");
      this._bar.selectItem(args.oldIndex);
    }
  }

  //Map events
  onMapReady(event) {
    console.log("Map Ready");

    this.mapView = event.object;
    this.mapView.setStyle(<Style>JSON.parse(this.styles));
    console.log("Setting a marker...");

    var marker = new Marker();
    marker.position = Position.positionFromLatLng(-33.86, 151.2);
    marker.title = "Sydney";
    marker.snippet = "Australia";
    marker.userData = { index: 1 };
    marker.icon = "x102icon";
    console.dir(marker);
    this.mapView.addMarker(marker);
  }

  onCoordinateTapped(args) {
    console.log(
      "Coordinate Tapped, Lat: " +
        args.position.latitude +
        ", Lon: " +
        args.position.longitude,
      args
    );
  }

  onMarkerEvent(args) {
    console.log(
      "Marker Event: '" +
        args.eventName +
        "' triggered on: " +
        args.marker.title +
        ", Lat: " +
        args.marker.position.latitude +
        ", Lon: " +
        args.marker.position.longitude,
      args
    );
  }

  onCameraChanged(args) {
    console.log(
      "Camera changed: " + JSON.stringify(args.camera),
      JSON.stringify(args.camera) === this.lastCamera
    );
    this.lastCamera = JSON.stringify(args.camera);
  }

  logIn() {
    this.loggedIn = !this.loggedIn;
  }
  public onCellSwiping(args: SwipeActionsEventData) {
    var swipeLimits = args.data.swipeLimits;
    var currentItemView = args.object;
    var currentView;

    if (args.data.x > 200) {
      console.log("Notify perform left action");
    } else if (args.data.x < -200) {
      console.log("Notify perform right action");
    }
  }

  public onSwipeCellStarted(args: SwipeActionsEventData) {
    var swipeLimits = args.data.swipeLimits;
    var listView = topmost().currentPage.getViewById("listView") as View;

    swipeLimits.threshold = listView.getMeasuredWidth();
    swipeLimits.left = listView.getMeasuredWidth();
    swipeLimits.right = listView.getMeasuredWidth();
  }

  public onSwipeCellFinished(args: SwipeActionsEventData) {
    if (args.data.x > 200) {
      console.log("Perform left action");
    } else if (args.data.x < -200) {
      console.log("Perform right action");
    }
  }

  public onItemClick(args: ListViewEventData) {
    var listView = <RadListView>topmost().currentPage.getViewById("listView");
    listView.notifySwipeToExecuteFinished();
    console.log("Item click: " + args.index);
  }

  public onLeftSwipeClick(args) {
    console.log("Left swipe click");
  }

  public onRightSwipeClick(args) {
    console.log("Right swipe click");
  }
}
