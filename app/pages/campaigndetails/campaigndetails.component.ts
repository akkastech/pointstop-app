import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { registerElement } from "nativescript-angular";
import { Page } from "ui/page";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Slider } from "ui/slider";
import * as TimeDatePicker from "nativescript-timedatepicker";

@Component({
  selector: "my-app",
  templateUrl: "pages/campaigndetails/campaigndetails.html",
  styleUrls: ["pages/campaigndetails/campaigndetails-common.css"]
})
export class CampaigndetailsComponent implements OnInit {
  @ViewChild("container") container: ElementRef;
  @ViewChild("CB1") FirstCheckBox: ElementRef;

  public points = 50;

  constructor(
    private router: Router,
    private page: Page,
    private routerExtensions: RouterExtensions
  ) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  public goBack() {
    this.routerExtensions.back();
  }

  public onSecondSliderChange(args) {
    let slider = <Slider>args.object;
    console.dir(slider);
    this.points = Math.round(slider.value);
  }
}
