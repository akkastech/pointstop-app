"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var CampaigndetailsComponent = (function () {
    function CampaigndetailsComponent(router, page, routerExtensions) {
        this.router = router;
        this.page = page;
        this.routerExtensions = routerExtensions;
        this.points = 50;
    }
    CampaigndetailsComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    CampaigndetailsComponent.prototype.goBack = function () {
        this.routerExtensions.back();
    };
    CampaigndetailsComponent.prototype.onSecondSliderChange = function (args) {
        var slider = args.object;
        console.dir(slider);
        this.points = Math.round(slider.value);
    };
    __decorate([
        core_1.ViewChild("container"),
        __metadata("design:type", core_1.ElementRef)
    ], CampaigndetailsComponent.prototype, "container", void 0);
    __decorate([
        core_1.ViewChild("CB1"),
        __metadata("design:type", core_1.ElementRef)
    ], CampaigndetailsComponent.prototype, "FirstCheckBox", void 0);
    CampaigndetailsComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "pages/campaigndetails/campaigndetails.html",
            styleUrls: ["pages/campaigndetails/campaigndetails-common.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router,
            page_1.Page,
            router_2.RouterExtensions])
    ], CampaigndetailsComponent);
    return CampaigndetailsComponent;
}());
exports.CampaigndetailsComponent = CampaigndetailsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FtcGFpZ25kZXRhaWxzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNhbXBhaWduZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUU7QUFFekUsZ0NBQStCO0FBQy9CLDBDQUF5QztBQUN6QyxzREFBK0Q7QUFTL0Q7SUFNRSxrQ0FDVSxNQUFjLEVBQ2QsSUFBVSxFQUNWLGdCQUFrQztRQUZsQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFMckMsV0FBTSxHQUFHLEVBQUUsQ0FBQztJQU1oQixDQUFDO0lBRUosMkNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztJQUNuQyxDQUFDO0lBRU0seUNBQU0sR0FBYjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRU0sdURBQW9CLEdBQTNCLFVBQTRCLElBQUk7UUFDOUIsSUFBSSxNQUFNLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQXZCdUI7UUFBdkIsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7a0NBQVksaUJBQVU7K0RBQUM7SUFDNUI7UUFBakIsZ0JBQVMsQ0FBQyxLQUFLLENBQUM7a0NBQWdCLGlCQUFVO21FQUFDO0lBRmpDLHdCQUF3QjtRQUxwQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLDRDQUE0QztZQUN6RCxTQUFTLEVBQUUsQ0FBQyxrREFBa0QsQ0FBQztTQUNoRSxDQUFDO3lDQVFrQixlQUFNO1lBQ1IsV0FBSTtZQUNRLHlCQUFnQjtPQVRqQyx3QkFBd0IsQ0F5QnBDO0lBQUQsK0JBQUM7Q0FBQSxBQXpCRCxJQXlCQztBQXpCWSw0REFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFNsaWRlciB9IGZyb20gXCJ1aS9zbGlkZXJcIjtcclxuaW1wb3J0ICogYXMgVGltZURhdGVQaWNrZXIgZnJvbSBcIm5hdGl2ZXNjcmlwdC10aW1lZGF0ZXBpY2tlclwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwibXktYXBwXCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwicGFnZXMvY2FtcGFpZ25kZXRhaWxzL2NhbXBhaWduZGV0YWlscy5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJwYWdlcy9jYW1wYWlnbmRldGFpbHMvY2FtcGFpZ25kZXRhaWxzLWNvbW1vbi5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIENhbXBhaWduZGV0YWlsc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQFZpZXdDaGlsZChcImNvbnRhaW5lclwiKSBjb250YWluZXI6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcIkNCMVwiKSBGaXJzdENoZWNrQm94OiBFbGVtZW50UmVmO1xyXG5cclxuICBwdWJsaWMgcG9pbnRzID0gNTA7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgcGFnZTogUGFnZSxcclxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9uc1xyXG4gICkge31cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnb0JhY2soKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMuYmFjaygpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uU2Vjb25kU2xpZGVyQ2hhbmdlKGFyZ3MpIHtcclxuICAgIGxldCBzbGlkZXIgPSA8U2xpZGVyPmFyZ3Mub2JqZWN0O1xyXG4gICAgY29uc29sZS5kaXIoc2xpZGVyKTtcclxuICAgIHRoaXMucG9pbnRzID0gTWF0aC5yb3VuZChzbGlkZXIudmFsdWUpO1xyXG4gIH1cclxufVxyXG4iXX0=