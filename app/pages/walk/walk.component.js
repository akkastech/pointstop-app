"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var element_registry_1 = require("nativescript-angular/element-registry");
var router_1 = require("@angular/router");
element_registry_1.registerElement("Carousel", function () { return require("nativescript-carousel").Carousel; });
element_registry_1.registerElement("CarouselItem", function () { return require("nativescript-carousel").CarouselItem; });
var WalkComponent = (function () {
    function WalkComponent(router, page) {
        this.router = router;
        this.page = page;
        this.isLast = false;
    }
    WalkComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    WalkComponent.prototype.agreePage = function () {
        if (this.isLast) {
            if (this.FirstCheckBox.nativeElement.checked) {
                this.router.navigate(["/home"]);
            }
            else {
                alert("Agree with terms & conditions first!");
            }
        }
        else {
            this.mainCarousel.nativeElement.selectedPage = 4;
            //this.router.navigate(["/agree"]);
        }
    };
    WalkComponent.prototype.myChangeEvent = function (args) {
        if (args.index == 4) {
            this.isLast = true;
        }
        else {
            this.isLast = false;
        }
        console.dir(args.index);
    };
    WalkComponent.prototype.homePage = function () {
        console.log(this.FirstCheckBox.nativeElement.checked);
        if (this.FirstCheckBox.nativeElement.checked) {
            this.router.navigate(["/home"]);
        }
        else {
            alert("Agree with terms & conditions first!");
        }
    };
    __decorate([
        core_1.ViewChild("container"),
        __metadata("design:type", core_1.ElementRef)
    ], WalkComponent.prototype, "container", void 0);
    __decorate([
        core_1.ViewChild("CB1"),
        __metadata("design:type", core_1.ElementRef)
    ], WalkComponent.prototype, "FirstCheckBox", void 0);
    __decorate([
        core_1.ViewChild("Carousel"),
        __metadata("design:type", core_1.ElementRef)
    ], WalkComponent.prototype, "mainCarousel", void 0);
    WalkComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            templateUrl: "pages/walk/walk.html",
            styleUrls: ["pages/walk/walk.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router, page_1.Page])
    ], WalkComponent);
    return WalkComponent;
}());
exports.WalkComponent = WalkComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Fsay5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ3YWxrLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUN6RSxnQ0FBK0I7QUFDL0IsMEVBQXdFO0FBQ3hFLDBDQUF5QztBQUV6QyxrQ0FBZSxDQUFDLFVBQVUsRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUMsUUFBUSxFQUF6QyxDQUF5QyxDQUFDLENBQUM7QUFDN0Usa0NBQWUsQ0FDYixjQUFjLEVBQ2QsY0FBTSxPQUFBLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLFlBQVksRUFBN0MsQ0FBNkMsQ0FDcEQsQ0FBQztBQU9GO0lBS0UsdUJBQW9CLE1BQWMsRUFBVSxJQUFVO1FBQWxDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBSnRELFdBQU0sR0FBWSxLQUFLLENBQUM7SUFJaUMsQ0FBQztJQUUxRCxnQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7SUFFRCxpQ0FBUyxHQUFUO1FBQ0UsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNoRCxDQUFDO1FBQ0gsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztZQUNqRCxtQ0FBbUM7UUFDckMsQ0FBQztJQUNILENBQUM7SUFFTSxxQ0FBYSxHQUFwQixVQUFxQixJQUFJO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNyQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN0QixDQUFDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3RELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO1FBQ2hELENBQUM7SUFDSCxDQUFDO0lBdEN1QjtRQUF2QixnQkFBUyxDQUFDLFdBQVcsQ0FBQztrQ0FBWSxpQkFBVTtvREFBQztJQUM1QjtRQUFqQixnQkFBUyxDQUFDLEtBQUssQ0FBQztrQ0FBZ0IsaUJBQVU7d0RBQUM7SUFDckI7UUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7a0NBQWUsaUJBQVU7dURBQUM7SUFKckMsYUFBYTtRQUx6QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztTQUNuQyxDQUFDO3lDQU00QixlQUFNLEVBQWdCLFdBQUk7T0FMM0MsYUFBYSxDQXlDekI7SUFBRCxvQkFBQztDQUFBLEFBekNELElBeUNDO0FBekNZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xyXG5pbXBvcnQgeyByZWdpc3RlckVsZW1lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5yZWdpc3RlckVsZW1lbnQoXCJDYXJvdXNlbFwiLCAoKSA9PiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWNhcm91c2VsXCIpLkNhcm91c2VsKTtcclxucmVnaXN0ZXJFbGVtZW50KFxyXG4gIFwiQ2Fyb3VzZWxJdGVtXCIsXHJcbiAgKCkgPT4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1jYXJvdXNlbFwiKS5DYXJvdXNlbEl0ZW1cclxuKTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm15LWFwcFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcInBhZ2VzL3dhbGsvd2Fsay5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCJwYWdlcy93YWxrL3dhbGsuY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBXYWxrQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBpc0xhc3Q6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBAVmlld0NoaWxkKFwiY29udGFpbmVyXCIpIGNvbnRhaW5lcjogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwiQ0IxXCIpIEZpcnN0Q2hlY2tCb3g6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcIkNhcm91c2VsXCIpIG1haW5DYXJvdXNlbDogRWxlbWVudFJlZjtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBhZ3JlZVBhZ2UoKSB7XHJcbiAgICBpZiAodGhpcy5pc0xhc3QpIHtcclxuICAgICAgaWYgKHRoaXMuRmlyc3RDaGVja0JveC5uYXRpdmVFbGVtZW50LmNoZWNrZWQpIHtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvaG9tZVwiXSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgYWxlcnQoXCJBZ3JlZSB3aXRoIHRlcm1zICYgY29uZGl0aW9ucyBmaXJzdCFcIik7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubWFpbkNhcm91c2VsLm5hdGl2ZUVsZW1lbnQuc2VsZWN0ZWRQYWdlID0gNDtcclxuICAgICAgLy90aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvYWdyZWVcIl0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG15Q2hhbmdlRXZlbnQoYXJncykge1xyXG4gICAgaWYgKGFyZ3MuaW5kZXggPT0gNCkge1xyXG4gICAgICB0aGlzLmlzTGFzdCA9IHRydWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmlzTGFzdCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5kaXIoYXJncy5pbmRleCk7XHJcbiAgfVxyXG4gIFxyXG4gIGhvbWVQYWdlKCkge1xyXG4gICAgY29uc29sZS5sb2codGhpcy5GaXJzdENoZWNrQm94Lm5hdGl2ZUVsZW1lbnQuY2hlY2tlZCk7XHJcbiAgICBpZiAodGhpcy5GaXJzdENoZWNrQm94Lm5hdGl2ZUVsZW1lbnQuY2hlY2tlZCkge1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvaG9tZVwiXSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBhbGVydChcIkFncmVlIHdpdGggdGVybXMgJiBjb25kaXRpb25zIGZpcnN0IVwiKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19