import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Page } from "ui/page";
import { registerElement } from "nativescript-angular/element-registry";
import { Router } from "@angular/router";

registerElement("Carousel", () => require("nativescript-carousel").Carousel);
registerElement(
  "CarouselItem",
  () => require("nativescript-carousel").CarouselItem
);

@Component({
  selector: "my-app",
  templateUrl: "pages/walk/walk.html",
  styleUrls: ["pages/walk/walk.css"]
})
export class WalkComponent implements OnInit {
  isLast: boolean = false;
  @ViewChild("container") container: ElementRef;
  @ViewChild("CB1") FirstCheckBox: ElementRef;
  @ViewChild("Carousel") mainCarousel: ElementRef;
  constructor(private router: Router, private page: Page) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  agreePage() {
    if (this.isLast) {
      if (this.FirstCheckBox.nativeElement.checked) {
        this.router.navigate(["/home"]);
      } else {
        alert("Agree with terms & conditions first!");
      }
    } else {
      this.mainCarousel.nativeElement.selectedPage = 4;
      //this.router.navigate(["/agree"]);
    }
  }

  public myChangeEvent(args) {
    if (args.index == 4) {
      this.isLast = true;
    } else {
      this.isLast = false;
    }
    console.dir(args.index);
  }
  
  homePage() {
    console.log(this.FirstCheckBox.nativeElement.checked);
    if (this.FirstCheckBox.nativeElement.checked) {
      this.router.navigate(["/home"]);
    } else {
      alert("Agree with terms & conditions first!");
    }
  }
}
