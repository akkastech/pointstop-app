import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Page } from "ui/page";
import { Router } from "@angular/router";

@Component({
  selector: "my-app",
  templateUrl: "pages/agree/agree.html",
  styleUrls: ["pages/agree/agree-common.css"]
})
export class AgreeComponent implements OnInit {
  @ViewChild("container") container: ElementRef;
  @ViewChild("CB1") FirstCheckBox: ElementRef;
  constructor(private router: Router, private page: Page) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.page.backgroundImage = "res://tscreen5";
  }

  homePage() {
    console.log(this.FirstCheckBox.nativeElement.checked);
    if (this.FirstCheckBox.nativeElement.checked) {
      this.router.navigate(["/home"]);
    } else {
      alert("Agree with terms & conditions first!");
    }
  }
}
