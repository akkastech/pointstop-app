import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Phone } from "./phone";
//import { PhoneService } from "./phone.service";
import { Router } from "@angular/router";
import { Page } from "ui/page";
import { Color } from "color";
import { View } from "ui/core/view";

@Component({
  selector: "my-app",
  //providers: [PhoneService],
  templateUrl: "pages/phone/phone.html",
  styleUrls: ["pages/phone/phone-common.css", "pages/phone/phone.css"]
})
export class PhoneComponent implements OnInit {
  phone: Phone;
  number: string;
  isSMS = false;
  @ViewChild("container") container: ElementRef;

  constructor(
    private router: Router,
    //private userService: UserService,
    private page: Page
  ) {
    this.phone = new Phone();
    this.phone.number = "03334546335";
  }

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.page.backgroundImage = "res://phonebg";
  }

  submit() {
    if (this.isSMS) {
      //alert(this.phone.number);
      this.router.navigate(["/walk"]);
    } else {
      this.isSMS = !this.isSMS;
    }
  }

  resend() {
    alert("SMS Resent");
  }
}
