"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var phone_1 = require("./phone");
//import { PhoneService } from "./phone.service";
var router_1 = require("@angular/router");
var page_1 = require("ui/page");
var PhoneComponent = (function () {
    function PhoneComponent(router, 
        //private userService: UserService,
        page) {
        this.router = router;
        this.page = page;
        this.isSMS = false;
        this.phone = new phone_1.Phone();
        this.phone.number = "03334546335";
    }
    PhoneComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundImage = "res://phonebg";
    };
    PhoneComponent.prototype.submit = function () {
        if (this.isSMS) {
            //alert(this.phone.number);
            this.router.navigate(["/walk"]);
        }
        else {
            this.isSMS = !this.isSMS;
        }
    };
    PhoneComponent.prototype.resend = function () {
        alert("SMS Resent");
    };
    __decorate([
        core_1.ViewChild("container"),
        __metadata("design:type", core_1.ElementRef)
    ], PhoneComponent.prototype, "container", void 0);
    PhoneComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            //providers: [PhoneService],
            templateUrl: "pages/phone/phone.html",
            styleUrls: ["pages/phone/phone-common.css", "pages/phone/phone.css"]
        }),
        __metadata("design:paramtypes", [router_1.Router,
            page_1.Page])
    ], PhoneComponent);
    return PhoneComponent;
}());
exports.PhoneComponent = PhoneComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGhvbmUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGhvbmUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLGlDQUFnQztBQUNoQyxpREFBaUQ7QUFDakQsMENBQXlDO0FBQ3pDLGdDQUErQjtBQVUvQjtJQU1FLHdCQUNVLE1BQWM7UUFDdEIsbUNBQW1DO1FBQzNCLElBQVU7UUFGVixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBRWQsU0FBSSxHQUFKLElBQUksQ0FBTTtRQU5wQixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBUVosSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQztJQUNwQyxDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7SUFDOUMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNmLDJCQUEyQjtZQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFDbEMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0IsQ0FBQztJQUNILENBQUM7SUFFRCwrQkFBTSxHQUFOO1FBQ0UsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUEzQnVCO1FBQXZCLGdCQUFTLENBQUMsV0FBVyxDQUFDO2tDQUFZLGlCQUFVO3FEQUFDO0lBSm5DLGNBQWM7UUFOMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxRQUFRO1lBQ2xCLDRCQUE0QjtZQUM1QixXQUFXLEVBQUUsd0JBQXdCO1lBQ3JDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixFQUFFLHVCQUF1QixDQUFDO1NBQ3JFLENBQUM7eUNBUWtCLGVBQU07WUFFUixXQUFJO09BVFQsY0FBYyxDQWdDMUI7SUFBRCxxQkFBQztDQUFBLEFBaENELElBZ0NDO0FBaENZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFBob25lIH0gZnJvbSBcIi4vcGhvbmVcIjtcclxuLy9pbXBvcnQgeyBQaG9uZVNlcnZpY2UgfSBmcm9tIFwiLi9waG9uZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XHJcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcImNvbG9yXCI7XHJcbmltcG9ydCB7IFZpZXcgfSBmcm9tIFwidWkvY29yZS92aWV3XCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJteS1hcHBcIixcclxuICAvL3Byb3ZpZGVyczogW1Bob25lU2VydmljZV0sXHJcbiAgdGVtcGxhdGVVcmw6IFwicGFnZXMvcGhvbmUvcGhvbmUuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wicGFnZXMvcGhvbmUvcGhvbmUtY29tbW9uLmNzc1wiLCBcInBhZ2VzL3Bob25lL3Bob25lLmNzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGhvbmVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHBob25lOiBQaG9uZTtcclxuICBudW1iZXI6IHN0cmluZztcclxuICBpc1NNUyA9IGZhbHNlO1xyXG4gIEBWaWV3Q2hpbGQoXCJjb250YWluZXJcIikgY29udGFpbmVyOiBFbGVtZW50UmVmO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAvL3ByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBwYWdlOiBQYWdlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnBob25lID0gbmV3IFBob25lKCk7XHJcbiAgICB0aGlzLnBob25lLm51bWJlciA9IFwiMDMzMzQ1NDYzMzVcIjtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcbiAgICB0aGlzLnBhZ2UuYmFja2dyb3VuZEltYWdlID0gXCJyZXM6Ly9waG9uZWJnXCI7XHJcbiAgfVxyXG5cclxuICBzdWJtaXQoKSB7XHJcbiAgICBpZiAodGhpcy5pc1NNUykge1xyXG4gICAgICAvL2FsZXJ0KHRoaXMucGhvbmUubnVtYmVyKTtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL3dhbGtcIl0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pc1NNUyA9ICF0aGlzLmlzU01TO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVzZW5kKCkge1xyXG4gICAgYWxlcnQoXCJTTVMgUmVzZW50XCIpO1xyXG4gIH1cclxufVxyXG4iXX0=